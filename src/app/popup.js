// Open a communications port to the background script, named 'popup' for handling purposes.
const p = chrome.runtime.connect({ name: 'popup' });

// When an <li> element is clicked, post a message to the background script with the selected items information.
$(document).on('click', 'li', function(e) {
	p.postMessage({action: $(this).attr('action'), data: $(this).attr('function')});
	window.close();
});

// When the options link text is clicked, open the options page.
$('a.options').on('click', function(e) {
	chrome.runtime.openOptionsPage();
});

$('a.how-to-use').on('click', function(e) {
	chrome.tabs.create({ url: 'https://secure.utas.edu.au/faculty-of-health-intranet/mylo-resources/mylo-mate/user-guide' });
});

$('a.whats-new').on('click', function(e) {
	chrome.tabs.create({ url: 'https://secure.utas.edu.au/faculty-of-health-intranet/mylo-resources/mylo-mate/#whatsnew' });
});

// Runs on document ready
$(function() {
	// Set the app text, including the current version.
	$('#app_title').text(chrome.runtime.getManifest().name+' v'+chrome.runtime.getManifest().version);
	document.querySelector('.whats-new').innerText += chrome.runtime.getManifest().version;
	
	// If the `other.showDev2` value is enabled, show the DEV2 list item.
	chrome.storage.sync.get({
		other: { showDev2: false }
	}, function(items) { 
		$('li[function="DEV2"]').toggle(items.other.showDev2);
	});
});