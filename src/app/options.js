/*global UI,Group,Groups,User,_,defaults,devDefaults*/
/** @namespace Chrome/Options */
$( function() {
	$( '#tabs' ).tabs({
		//heightStyle: 'auto' // This isn't working anymore, and I'm not sure why. I'm leaving this property here for legacy purposes, in case it can be fixed again.
	});
} );

if (window._ === undefined) window._ = (...rest) => document.querySelector(rest);

// A reference to the confirmation dialog.
const dev_confirmdialog = _('#confirmDevOptions');
const defaults_confirmdialog = _('#confirmDefaults');


/**
 * Sets the defaults that an EdTech or EdDev would use, as opposed to a lecturer.
 * Once the settings have been saved, it will 'restore' these options to the users screen.
 * @memberof Chrome/Options#
 */
function setDevDefaults() {
	dev_confirmdialog.close();	
	chrome.storage.sync.get(null, (data) => {
		chrome.storage.sync.set({ ...defaults, ...devDefaults, usergroups: { ...data.usergroups, enabled: true }}, restore_options );
	});
}

/**
 * Sets the defaults to the default install state.
 * Once the settings have been saved, it will 'restore' these options to the users screen.
 * @memberof Chrome/Options#
 */
function setDefaults() {
	defaults_confirmdialog.close();	
	chrome.storage.sync.get(null, data => {
		chrome.storage.sync.set({ ...defaults,  usergroups: { ...data.usergroups, enabled: defaults.usergroups.enabled } }, restore_options );
	});
}

/**
 * Stores the form options into the chrome storage area, then closes the options panel. 
 * @memberof Chrome/Options#
 */
function save_options() {  
	
	chrome.storage.sync.get(null, (data) => {
		chrome.storage.sync.set({
			manager: {
				adjustTable: _('#mm_adjustTable').checked,
				enablePending: _('#mm_enablePending').checked,
				recordsPerPage: _('#mm_recordsPerPage').value,
				addQuicklinks: _('#mm_addQuicklinks').checked,
				addBUM: _('#mm_addBUM').checked,
				displayRowColours: _('#mm_displayRowColours').checked
			},
			mylo: {
				adjustButtons: _('#m_adjustButtons').checked,
				enhancements: _('#m_enhancements').checked,
				overrideGrades: _('#m_overrideGrades').checked,
				enableUnitOutline: _('#m_enableUnitOutline').checked,
				highlightRowBackgrounds: _('#m_highlightRowBackgrounds').checked,
				floatingRubricEditors: _('#m_floatingRubricEditors').checked
			},
			editor: {
				defaultShow: _('#editor_defaultShow').checked,
				showDevTools: _('#editor_showDevTools').checked,
				keepSmall: _('#editor_keepSmall').checked,
				enablePreviewWindow: _('#editor_enablePreviewWindow').checked,
				allLinksInNewTab: _('#editor_allLinksInNewTab').checked
			},
			other: {
				showDev2: _('#other_showDev2').checked,
				makeAccessible: _('#other_makeAccessible').checked
			},
			usergroups: {
				enabled: _('#mm_displayUserGroups').checked,
				groups: data.usergroups.groups
			}
		}, cleanup);
	});
	
}

// Closes the options panel.
function cleanup() {  
	window.close(); 
}

/**
 * Restores the users current settings to the options form.
 * @memberof Chrome/Options#
 */
function restore_options() {  
	chrome.storage.sync.get(null, function(items) {
		items = $.extend(true, {}, defaults, items);
		_('#mm_adjustTable').checked = items.manager.adjustTable;
		_('#mm_enablePending').checked = items.manager.enablePending;
		_('#mm_recordsPerPage').value = items.manager.recordsPerPage;
		_('#mm_addQuicklinks').checked = items.manager.addQuicklinks;
		_('#mm_addBUM').checked = items.manager.addBUM;
		_('#mm_displayRowColours').checked = items.manager.displayRowColours;
		
		_('#m_adjustButtons').checked = items.mylo.adjustButtons;
		_('#m_enhancements').checked = items.mylo.enhancements;
		_('#m_overrideGrades').checked = items.mylo.overrideGrades;
		_('#m_enableUnitOutline').checked = items.mylo.enableUnitOutline;
		_('#m_highlightRowBackgrounds').checked = items.mylo.highlightRowBackgrounds;
		_('#m_floatingRubricEditors').checked = items.mylo.floatingRubricEditors;
		
		_('#editor_defaultShow').checked = items.editor.defaultShow;
		_('#editor_showDevTools').checked = items.editor.showDevTools;
		_('#editor_keepSmall').checked = items.editor.keepSmall;
		_('#editor_enablePreviewWindow').checked = items.editor.enablePreviewWindow;
		_('#editor_allLinksInNewTab').checked = items.editor.allLinksInNewTab;

		_('#other_showDev2').checked = items.other.showDev2;
		_('#other_makeAccessible').checked = items.other.makeAccessible;

		_('#mm_displayUserGroups').checked = items.usergroups.enabled;

		_('li.usergroups').style.display  = items.usergroups.enabled ? 'block' : 'none';
		document.dispatchEvent(new CustomEvent('Ready', { detail: { data: items } }));
	});
}

// When the content loads, restore the current settings.
document.addEventListener('DOMContentLoaded', restore_options);

// When 'save' is clicked, save the settings into the storage area.
_('#save').addEventListener('click', save_options);

// If the 'Developer Defaults' button is selected, open the dialog.
_('#dev-enabled').addEventListener('click', function() { dev_confirmdialog.showModal(); });

// When the 'OK' button is clicked in the Developer Options dialog, set the developer defaults.
_('#confirmDevOptions .ok').addEventListener('click', setDevDefaults);

// If the 'Close' button is clicked in the Developer Options dialog, close the dialog.
_('#confirmDevOptions .cancel').addEventListener('click', function() { dev_confirmdialog.close(); });

// If the 'Defaults' button is selected, open the dialog.
_('#defaults').addEventListener('click', function() { defaults_confirmdialog.showModal(); });

// When the 'OK' button is clicked in the Options dialog, set the defaults.
_('#confirmDefaults .ok').addEventListener('click', setDefaults);

// If the 'Close' button is clicked in the Options dialog, close the dialog.
_('#confirmDefaults .cancel').addEventListener('click', function() { defaults_confirmdialog.close(); });

_('#mm_displayUserGroups').addEventListener('click', (e) => {
	_('li.usergroups').style.display = e.target.checked ? 'block' : 'none';
});