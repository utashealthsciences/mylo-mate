var UserRoles = ['Lecturer', 'Student', 'Support', 'Tutor', 'Auditor'];

/**
 *  UI TOOLS
 */

var UI = {
	get container() {
		return document.body;
	},
	prompt(title, desc, placeholder, defaultValue) {
		return new Promise((resolve, reject) => {
			let dialog = document.createElement('dialog');
			dialog.classList.add('dialog');
			dialog.classList.add('dialog-prompt');

			let name = document.createElement('p');
			name.className = 'name';
			name.innerText = title || '';

			let info = document.createElement('div');
			info.className = 'info';
			info.innerHTML = desc || '';

			let input = document.createElement('input');
			input.type = 'text';
			input.placeholder = placeholder || '';
			input.value = defaultValue || '';

			input.addEventListener('keyup', (e) => { if (e.keyCode === 13) { dialog.close(); resolve(input.value); document.body.removeChild(dialog); } });

			let dialog_yes = document.createElement('button');
			dialog_yes.innerText = 'OK';
			dialog_yes.classList.add('btn');
			dialog_yes.classList.add('okay_btn');
			dialog_yes.addEventListener('click', function () { dialog.close(); resolve(input.value); document.body.removeChild(dialog); });

			let dialog_no = document.createElement('button');
			dialog_no.innerText = 'Cancel';
			dialog_no.classList.add('btn');
			dialog_no.classList.add('cancel_btn');
			dialog_no.addEventListener('click', function () { dialog.close(); reject(); document.body.removeChild(dialog); });

			dialog.appendChild(name);
			dialog.appendChild(info);

			dialog.appendChild(input);

			dialog.appendChild(dialog_yes);
			dialog.appendChild(dialog_no);

			document.body.appendChild(dialog);

			dialog.showModal();
		});

	},
	alert(title, desc) {
		let dialog = document.createElement('dialog');
		dialog.classList.add('dialog');
		dialog.classList.add('dialog-alert');

		let name = document.createElement('p');
		name.className = 'name';
		name.innerText = title || '';

		let info = document.createElement('div');
		info.className = 'info';
		info.innerHTML = desc || '';

		let okay = document.createElement('button');
		okay.innerText = 'OK';
		okay.classList.add('btn');
		okay.classList.add('okay_btn');
		okay.addEventListener('click', function () { dialog.close(); document.body.removeChild(dialog); });

		dialog.appendChild(name);
		dialog.appendChild(info);
		dialog.appendChild(okay);

		document.body.appendChild(dialog);

		dialog.showModal();
	},
	confirm(title, desc, yes, no) {
		return new Promise((resolve, reject) => {
			let dialog = document.createElement('dialog');
			dialog.classList.add('dialog');
			dialog.classList.add('dialog-confirm');

			let name = document.createElement('p');
			name.className = 'name';
			name.innerText = title || '';

			let info = document.createElement('div');
			info.className = 'info';
			info.innerHTML = desc || '';

			let dialog_yes = document.createElement('button');
			dialog_yes.innerText = yes || 'OK';
			dialog_yes.classList.add('btn');
			dialog_yes.classList.add('okay_btn');
			dialog_yes.addEventListener('click', function () { dialog.close(); document.body.removeChild(dialog); resolve(); });

			let dialog_no = document.createElement('button');
			dialog_no.innerText = no || 'Cancel';
			dialog_no.classList.add('btn');
			dialog_no.classList.add('cancel_btn');
			dialog_no.addEventListener('click', function () { dialog.close(); document.body.removeChild(dialog); reject(); });

			dialog.appendChild(name);
			dialog.appendChild(info);

			dialog.appendChild(dialog_yes);
			dialog.appendChild(dialog_no);

			document.body.appendChild(dialog);

			dialog.showModal();
		});
	},
	notify(title, info, { classes='', timeout = 5, parent = this.container } = {}) {
		let n = document.createElement('div');
		n.className = 'visible ' + classes; n.id = 'notify';
		n.innerHTML = `<p class="title">${title}<span class="close" title="Dismiss message">x</span></p><p class="desc">${info}</p>`;
		parent.appendChild(n);

		let timer = setTimeout(() => n.classList.add('fade'), timeout * 1000);
		n.querySelector('.close').addEventListener('click', () => { clearTimeout(timer); parent.removeChild(n); });
		n.addEventListener('animationend', () => { parent.removeChild(n); });
	},
	promptRoles(title, desc, users) {
		return new Promise((resolve, reject) => {
			let dialog = document.createElement('dialog');
			dialog.classList.add('dialog');
			dialog.classList.add('dialog-prompt');

			let name = document.createElement('p');
			name.className = 'name';
			name.innerText = title || '';

			let info = document.createElement('div');
			info.className = 'info';
			info.innerHTML = desc || '';

			let table = document.createElement('table');
			table.className = 'userRoles';
			table.setAttribute('cellspacing', 0);

			let headers = document.createElement('tr');
			
			let headName = document.createElement('th');
			headName.innerText = 'User';
			headers.appendChild(headName);
		
			let headRole = document.createElement('th');
			headRole.innerText = 'Role';
			headers.appendChild(headRole);

			table.appendChild(headers);

			let roles = document.createElement('select');
			roles.className = 'roleSelect';
			UserRoles.forEach((role) => {
				let r = document.createElement('option');
				r.value = r.innerHTML = role;
				roles.appendChild(r);
			});

			users.forEach((user) => {
				let row = document.createElement('tr');

				let u = document.createElement('td');
				u.innerText = `${user.PreferredName} ${user.Surname} (${user.Username})`;

				let r = document.createElement('td');
				r.appendChild(roles.cloneNode(true));
				r.querySelector('select').setAttribute('user', user.Username);
				r.querySelector(`select option[value="${user.Role || 'Support'}"]`).selected = true;

				row.appendChild(u);
				row.appendChild(r);

				table.appendChild(row);
			});
			
			
			
			let dialog_done = document.createElement('button');
			dialog_done.innerText = 'Close';
			dialog_done.classList.add('btn');
			dialog_done.classList.add('okay_btn');
			dialog_done.addEventListener('click', function () { 
				dialog.close(); 
				let roles = [];
				document.querySelectorAll('.roleSelect').forEach((role) => {
					let user = users.filter(u => u.Username === role.getAttribute('user'))[0];
					user.Role = role.value;
					roles.push(user);
				});
				resolve(roles); 
				document.body.removeChild(dialog); 
			});

			dialog.appendChild(name);
			dialog.appendChild(info);

			dialog.appendChild(table);

			dialog.appendChild(dialog_done);

			document.body.appendChild(dialog);

			dialog.showModal();
		});

	},
	promptUserAdd(title, desc, placeholder, defaultValue, roleText) {
		return new Promise((resolve, reject) => {
			let dialog = document.createElement('dialog');
			dialog.classList.add('dialog');
			dialog.classList.add('dialog-prompt');
			
			
			let name = document.createElement('p');
			name.className = 'name';
			name.innerText = title || '';
			
			let info = document.createElement('div');
			info.className = 'info';
			info.innerHTML = desc || '';
			
			let input = document.createElement('input');
			input.type = 'text';
			input.placeholder = placeholder || '';
			input.value = defaultValue || '';
			
			input.addEventListener('keyup', (e) => { if (e.keyCode === 13) { dialog.close(); resolve({ users: input.value, role: roles.value }); document.body.removeChild(dialog); } });
			
			let role = document.createElement('p');
			role.innerHTML = roleText; 
			
			let roles = document.createElement('select');
			roles.style.display = 'inline-block';
			roles.innerHTML = UserRoles.map(role => `<option value="${role}"${role === 'Support' ? ' selected' : ''}>${role}</option>`).join('\n');
			
			role.appendChild(roles);

			let dialog_yes = document.createElement('button');
			dialog_yes.innerText = 'OK';
			dialog_yes.classList.add('btn');
			dialog_yes.classList.add('okay_btn');
			dialog_yes.addEventListener('click', function () { dialog.close(); resolve({ users: input.value, role: roles.value }); document.body.removeChild(dialog); });
			
			let dialog_no = document.createElement('button');
			dialog_no.innerText = 'Cancel';
			dialog_no.classList.add('btn');
			dialog_no.classList.add('cancel_btn');
			dialog_no.addEventListener('click', function () { dialog.close(); reject(); document.body.removeChild(dialog); });
			
			dialog.appendChild(name);
			dialog.appendChild(info);

			dialog.appendChild(input);

			dialog.appendChild(role);

			dialog.appendChild(dialog_yes);
			dialog.appendChild(dialog_no);


			document.body.appendChild(dialog);

			dialog.showModal();
		});

	},

};

function Group(id, name) {
	this.ID = id;
	this.Name = name;
	this.Users = [];
}

function User({ PreferredName, Surname, StaffID, Username, Schools, Role }) {
	this.StaffID = StaffID;
	this.Username = Username;
	this.PreferredName = PreferredName;
	this.Surname = Surname;
	this.Schools = Schools;
	this.Role = Role;
}

var Groups = {
	get current() {
		return document.getElementById('usergroups_groups').value;
	},
	get valid() {
		return this.current.trim() !== '';
	},
	get(id) {
		return new Promise((resolve) => chrome.storage.sync.get({ 'usergroups': null }, (data) => { if (id !== undefined) { resolve(data.usergroups.groups[id]); } else { resolve(data.usergroups.groups); } }));
	}
};

function GetDataFromStaffUsername(username) {
	return new Promise((resolve, reject) => {
		$.ajax({
			url: 'https://mylo-manager.utas.edu.au/User/StaffSearch?Surname=&GivenName=&Username=' + username + '&DepartmentCode=&StaffId=',
			dataType: 'json',
			type: 'POST',
			data: {
				_search: false,
				nd: 1501635126034,
				rows: 100,
				page: 1,
				sidx: 'givenname',
				sord: 'asc'
			},
			success: (data) => {
				// NOTE:
				// 		ALL STAFF DETAILS SHOULD BE RETREIVED BY STAFF USERNAME ONLY.
				//		THIS IS TO REMOVE THE RISK OF DUPLICATE RECORDS!
				if(data.total === 0) { resolve({success: false, username}); return; }
				data = data.rows.filter((row) => row.id == username)[0];
				if (data === undefined) { resolve({ success: false, username }); return; }

				resolve({
					id: data.id,
					PreferredName: data.cell[0],
					Surname: data.cell[1],
					Username: data.cell[2],
					StaffID: data.cell[3],
					Schools: data.cell[4],
					success: true
				});
			},
			error: reject
		});
	});
}

