/** @namespace Chrome/Actions */

/**
 * Returns a regexp string based on * wildcards in the argument.
 * 
 * @author Connor Deckers (cdeckers)
 * @param {String} rule 
 * @returns new RegExp
 * @memberof Chrome/Actions#
 */
function wildcard(rule) {
	return new RegExp('^' + rule.split('*').join('.*') + '$');
}

// The ID of the context-based actions in the popup panel.
const contextID = 'context';

let actions = [];

function GetData(store) {
	return new Promise(function(resolve, reject) {
		chrome.storage.sync.get(store, resolve);
	});
}

/**
 * Return the appropriate action set based on the given URL.
 * @param {string} url The URL to compare against.
 * @memberof Chrome/Actions#
 */
function getAction(url) {
	const u = new URL(url);

	for(let i=0; i<actions.length; i++) {
		const action = actions[i];
		if((action.url == u.hostname || (Array.isArray(action.url) && action.url.indexOf(u.hostname) > -1)) && (action.paths == undefined || action.paths.length == 0)) return action;
		else if ((action.url == u.hostname || (Array.isArray(action.url) && action.url.indexOf(u.hostname) > -1)) && action.paths && action.paths.length > 0) {
			for(let j=0; j<action.paths.length;j++) {
				if(u.pathname.match(wildcard(action.paths[j]))) return action;
			}
		}
	}	
	return null;
}

function DocumentReady() {
	
	// Queries the current tab, and attempts to get an action set for it.
	// If it does, then it will populate the contextual panel of the popup and unhide it.
	chrome.tabs.query({active:true, currentWindow:true}, (tab) => {
		const action = getAction(tab[0].url);
		if(action !== null) {
			action.actions.forEach((item) => { 
				let show = typeof item.enabled == 'boolean' ? item.enabled : true;			
				if(show) {
					const description = action.description ? ' <i aria-hidden="true" class="fa-help" title="' + action.description + '"></i>' : '';
					$('#'+contextID+' ul').append(`<li action="context" function="${item.data}">${item.title}${description}</li>`); 
				}
			});
			$('#'+contextID).removeClass('hidden');
		}
	});

}

// Let's run our code!
GetData({ 'editor': { 'allLinksInNewTab': false } }).then(function(data) {
	// The contextual actions to run, based upon the URL and path of the current tab.
	actions = [
		{
			url: ['mylo.utas.edu.au', 'd2ldev2.utas.edu.au'],
			paths: ['/d2l/le/content/*/authorHtml*', '/d2l/le/content/*/EditFile*'],
			actions: [
				{
					title: 'Toggle ICB Tools',
					data: 'mylo-icb',
				},
				{
					title: 'Open All Links in New Tab',
					data: 'all-links-in-new-tab',
					enabled: !data.editor.allLinksInNewTab
				}
			],
			description: 'Hide or show the ICB tool in the MyLO editor.'
		},
		{
			url: 'echo360.org.au',
			paths: ['/section/*/home'],
			actions: [
				{
					title: 'Download Clips',
					data: 'echo360-download',
				}
			],
			description: 'Download the currently visible clips to your local machine.'
		},
	];

	DocumentReady();

});