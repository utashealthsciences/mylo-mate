(function () {
	if (window.SearchWindowReady) return;
	window.SearchWindowReady = true;

	const now = new Date();

	const opt = () => new Promise(resolve => { 
		chrome.storage.sync.get(null, resolve);
	});

	const search = () => {
		let exam_days = 14; //see src/mylo-search/content.js for more information.
		const rows = document.querySelectorAll(document.body.classList.contains('daylight') ? '#wrapper > table > tbody > tr' : '#grid > div.d2l-grid-inner-wrapper > table > tbody > tr');
		opt().then(store => {
			rows.forEach(row => {		
				const cells = row.children;
				const start = new Date(cells[cells.length - 2].innerText);
				const end = new Date(cells[cells.length - 1].innerText);
				end.setDate(end.getDate() + exam_days);

				let rowChanged = true;

				if(now > start && now < end) row.classList.add('mylo-mate-current');
				else if(now > end) row.classList.add('mylo-mate-ended');		
				else rowChanged = false;

				// To make things accessable
				if (store.other.makeAccessible && rowChanged) row.classList.add('mylo-mate-accessible');
			});
		});
	};

	opt().then(store => {
		if(store.mylo.highlightRowBackgrounds) {
			const search_mut = new MutationObserver(search);
			search_mut.observe(document.querySelector('.d2l-placeholder.d2l-placeholder-inner.d2l-placeholder-live'), { childList: true, subtree: true });
			search();
		}
	});

})();