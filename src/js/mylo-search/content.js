(function() {
	if(window.SearchReady) return;
	window.SearchReady = true;

	const opt = () => new Promise(resolve => {
		chrome.storage.sync.get(null, resolve);
	});

	const daylight = document.body.classList.contains('daylight');

	opt().then(store => {
		if (store.mylo.highlightRowBackgrounds) {
			//#region Prototype extentions
			if(!Element.findParent) {
				Element.prototype.findParent = function(tag) {
					for (let elem = this; elem !== document; elem = elem.parentNode) {
						if(elem.tagName.toLowerCase() === tag.toLowerCase()) return elem;
					}
					return null;
				};
			}
			//#endregion

			//#region Variables

			const now = new Date();

			const d = {
				month: now.getMonth(),
				year: now.getFullYear() - 2000
			};

			const exam_days = 14; // The amount of days a unit should remain 'active' (green) after a semester.

			const sem_dates = {
				'1': {
					start: new Date(2017, 2, 27),
					end: new Date(2017, 6, 2)
				},
				'2': {
					start: new Date(2017, 7, 17),
					end: new Date(2017, 10, 20)
				},
				'YR': {
					start: new Date(2017, 2, 27),
					end: new Date(2017, 10, 21)
				}
			};

			const term_dates = {
				'1': {
					start: new Date(2017, 2, 6),
					end: new Date(2017, 4, 13)
				},
				'2': {
					start: new Date(2017, 5, 1),
					end: new Date(2017, 7, 7)
				},
				'3': {
					start: new Date(2017, 7, 24),
					end: new Date(2017, 9, 29)
				},
				'4': {
					start: new Date(2017, 10, 16),
					end: new Date(2017, 12, 22)
				}
			};
			//#endregion

			//#region Select a Unit.. Mutation Observer

			let mut = new MutationObserver(function (mutations) {
				opt().then(store => {
					document.querySelectorAll('#courseSelectorId .d2l-datalist-item').forEach(row => {
						const splits = row.innerText.split('-');
						const text = splits[splits.length - 1].trim();
						const regex = /_([0-9]{2})(YR|S[0-9]{1})_/;
						const result = text.match(regex);
					
						let rowChanged = false;

						if (result === null) return;
		
						const year = parseInt(result[1]);
						const part = result[2];
		
						const parent = row.findParent('li');
		
						if (year == d.year) {
							if (part.startsWith('S')) {
								let p = part.replace('S', '');
								let r = sem_dates[p];
								if (r !== undefined) {
									r.end.setDate(r.end.getDate() + exam_days);
									if (now > r.start && now < r.end) {
										parent.classList.add('mylo-mate-current');
										rowChanged = true;
									}
									else if (now > r.end) {
										parent.classList.add('mylo-mate-ended');
										rowChanged = true;
									}
								}
							} else if (part.startsWith('T')) {
								let p = part.replace('T', '');
								let r = term_dates[p];
								r.end.setDate(r.end.getDate() + exam_days);
								if (r !== undefined) {
									if (now > r.start && now < r.end) {
										parent.classList.add('mylo-mate-current');
										rowChanged = true;
									}
									else if (now > r.end) {
										parent.classList.add('mylo-mate-ended');
										rowChanged = true;
									}
								}
							} else if(part.includes('YR')) {
								parent.classList.add('mylo-mate-current');
								rowChanged = true;
							}
						}
						else if (year < d.year) {
							parent.classList.add('mylo-mate-ended');
							rowChanged = true;
						}

						// To make things accessable
						if (store.other.makeAccessible && rowChanged) parent.classList.add('mylo-mate-accessible');
					});
				});
			});

			mut.observe(document.querySelector(daylight ? 'd2l-dropdown-content' : '.d2l-menuflyout-group-container'), { childList: true, subtree: true });

			//#endregion

			//#region Last 25 Accessed Units
			const last25 = () => {
				const last25box = document.querySelector('.d2l-action-container');
				if(last25box !== null) {
					opt().then(store => {
						last25box.parentNode.querySelectorAll('.d2l-datalist div.d2l-datalist-item-content div.d2l-textblock.d2l-textblock-secondary').forEach(row => {
							const timeframe = row.innerText.split(',')[0].trim();

							if (timeframe === undefined) return;
							if (timeframe.includes('Ongoing')) return;
						
							const parent = row.findParent('li');
							const year = timeframe.substr(0, 4);
				
							let rowChanged = false;

							if (parseInt(year) > now.getFullYear()) return;
							else if (parseInt(year) < now.getFullYear())  {
								parent.classList.add('mylo-mate-ended');
								rowChanged = true;
							}
							else if (timeframe.includes('YR') && parseInt(year) == now.getFullYear()) {
								parent.classList.add('mylo-mate-current');
								rowChanged = true;
							}
							else if (timeframe.includes('Sem')) { 
								let sem = sem_dates[timeframe.split('Sem')[1].trim()]; 
								if(!sem) return;
								sem.end.setDate(sem.end.getDate() + exam_days);
								if (now > sem.start && now < sem.end) { 
									parent.classList.add('mylo-mate-current'); 
									rowChanged = true;
								}
								else if(now > sem.end) { 
									parent.classList.add('mylo-mate-ended'); 
									rowChanged = true;
								}
							}
							else if (timeframe.includes('Term')) {
								let term = term_dates[timeframe.split('Term')[1].trim()];
								if(!term) return;
								term.end.setDate(term.end.getDate() + exam_days);
								if (now > term.start && now < term.end) {
									parent.classList.add('mylo-mate-current'); 
									rowChanged = true;
								}
								else if (now > term.end) { 
									parent.classList.add('mylo-mate-ended'); 
									rowChanged = true;
								}
							}	

							// To make things accessable
							if (store.other.makeAccessible && rowChanged) parent.classList.add('mylo-mate-accessible');
						});
					});
				}	
			};
		
			const parent = document.querySelector('.d2l-placeholder.d2l-placeholder-inner.d2l-placeholder-live');
			if(parent !== null) {
				const last25mut = new MutationObserver(last25);
				last25mut.observe(parent, { childList: true, subtree: true });
			}
			//#endregion
			
		}		
	});
})();