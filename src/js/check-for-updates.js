var showUpdateNotificationPanel = true; // Set to true to show notification panel with this update
let allowed = []; chrome.permissions.getAll(p => allowed = p.origins);
function matches(url, urls) { for (var i = 0; i < urls.length; i++) { if (url.match(wildcard(urls[i]))) return true; } }
function wildcard(rule) { return new RegExp('^' + rule.split('*').join('.*') + '$'); }

chrome.runtime.onInstalled.addListener(details => {
	switch (details.reason) {
		case 'install':
			// We need to replace this one with a notification popup as well.
			// We need to talk about it though, as if there aren't any windows we can hook into already open, no popup will appear!
			// In the mean time, it'll be easier just keeping the 'open a new tab' method.
			chrome.tabs.create({ url: 'https://secure.utas.edu.au/faculty-of-health-intranet/mylo-resources/mylo-mate/' });
			break;
		case 'update':
			if (details.previousVersion !== chrome.runtime.getManifest().version) {
				// This is just for the new update, and should be removed once officially rolled out.
				// It'll flush old unit outline data, which now has a new structure (and we don't want to return dead values!)
				chrome.storage.local.clear();
				if (showUpdateNotificationPanel) {
					// chrome.tabs.create({ url: 'https://secure.utas.edu.au/faculty-of-health-intranet/mylo-resources/mylo-mate/#whatsnew' });
					chrome.tabs.query({}, tabs => tabs.filter(tab => matches(tab.url, allowed)).forEach(tab => {
						chrome.tabs.executeScript(tab.id, { file: 'js/new-update.js' });
					}));
				}

				// Overlay our existing settings onto our settings structure, 
				//	clear the existing settings and then apply our fresh structure (with the existing settings in place!)
				exportSettings().then(store => {
					chrome.storage.sync.clear(() => {
						chrome.storage.sync.set(store);
					});
				});
			}
			break;
	}
});

chrome.runtime.onMessage.addListener(function (r) {
	if (r.request == 'closeNotification') {
		chrome.tabs.query({}, tabs => tabs.filter(tab => matches(tab.url, allowed)).forEach(tab => {
			chrome.tabs.sendMessage(tab.id, { command: 'close' });
		}));
	}
});

function openADialog() {
	chrome.tabs.query({ active: true, currentWindow: true }, tab => {
		chrome.tabs.executeScript(tab[0].id, { file: 'js/new-update.js' });
	});
}

function apply(structure, data) {
	let o = {};
	function iterate(a, b, base = o) {
		if (!a || !b) return;
		for (let k in a) {
			if (a.hasOwnProperty(k)) {
				if (typeof a[k] === 'object') {
					base[k] = base[k] || {};
					if (a[k]['_raw']) { base[k] = { ...a[k], ...b[k] }; }
					else iterate(a[k], b[k], base[k]);
				} else {
					base[k] = b.hasOwnProperty(k) ? b[k] : a[k];
				}
			}
		}
	}
	iterate(structure, data);
	return o;
}

function getStore() {
	return new Promise(resolve => {
		const xhr = new XMLHttpRequest();
		xhr.open('GET', chrome.runtime.getURL('config/store.js'));

		xhr.onreadystatechange = function () {
			if (xhr.readyState === 4 && xhr.status === 200) {
				const res = xhr.responseText;
				const obj = 'defaults';

				let rx = new RegExp(obj + '\\s*?=\\s*?(\{[^]+?\});', 'gi');
				let item = rx.exec(res);
				resolve(JSON.parse(item[1]));
			}
		};
		xhr.send(null);
	});
}

function exportSettings() {
	return new Promise(resolve => {
		chrome.storage.sync.get(null, current => {
			getStore().then(defaults => {
				resolve(apply(defaults, current));
			});
		});
	})
}