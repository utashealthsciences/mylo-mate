(function() {
	if(window.ManageFilesFix) return; 
	
	const items = document.querySelector('ul.da_a');
	if (items != null) window.ManageFilesFix = true;
	else return;

	const addItem = ({id, title}) => {
		const item = document.createElement('li');
		item.className = 'da_ai float_l da_icon ' + id;
		item.style.display = 'inline';
		item.innerHTML = `<a role="button" tabindex="0" title="${title}"><i class="checkbox"></i></a>`;
		items.appendChild(item);
		return item;
	};

	const folder = addItem({ id: 'folder', title: 'Select / Unselect all folders' });
	const linked = addItem({ id: 'linked', title: 'Select / Unselect all linked files' });
	const unlinked = addItem({ id: 'unlinked', title: 'Select / Unselect all unlinked files' });
	
	folder.addEventListener('click', () => {
		const rows = [...document.querySelectorAll('.yui-dt-data tr')].filter(row => row.querySelector('.d_dg_col_Type span').innerText === 'Folder');
		const checkedRows = [...document.querySelectorAll('.yui-dt-selected')].filter(row => row.querySelector('.d_dg_col_Type span').innerText === 'Folder');
	
		rows.forEach(row => {
			if(rows.length != checkedRows.length) {
				row.querySelector('input[type="checkbox"]').checked = true;
				row.classList.add('yui-dt-selected');
				row.querySelector('input[type="checkbox"]').dispatchEvent(new Event('click'));
			}
			else {				
				row.querySelector('input[type="checkbox"]').checked = false;
				row.classList.remove('yui-dt-selected');
				row.querySelector('input[type="checkbox"]').dispatchEvent(new Event('click'));
			}
		});	
	});

	linked.addEventListener('click', () => {
		const rows = [...document.querySelectorAll('.yui-dt-data tr')].filter(row => row.querySelectorAll('img[src^="/d2l/img/0/LE_PT_ManageFiles.Main.infTopic.gif"]').length === 1);
		const checkedRows = [...document.querySelectorAll('.yui-dt-selected')].filter(r => r.querySelector('img[src^="/d2l/img/0/LE_PT_ManageFiles.Main.infTopic.gif"]'));

		rows.forEach(row => {
			if(rows.length != checkedRows.length) {
				row.querySelector('input[type="checkbox"]').checked = true;
				row.classList.add('yui-dt-selected');
				row.querySelector('input[type="checkbox"]').dispatchEvent(new Event('click'));
			}
			else {				
				row.querySelector('input[type="checkbox"]').checked = false;
				row.classList.remove('yui-dt-selected');
				row.querySelector('input[type="checkbox"]').dispatchEvent(new Event('click'));
			}
		});
	});
	
	unlinked.addEventListener('click', () => {
		const rows = [...document.querySelectorAll('.yui-dt-data tr')].filter(row => row.querySelector('.d_dg_col_Type span').innerText !== 'Folder' && row.querySelectorAll('img[src^="/d2l/img/0/LE_PT_ManageFiles.Main.infTopic.gif"]').length === 0);
		const checkedRows = [...document.querySelectorAll('.yui-dt-selected')].filter(row => row.querySelector('.d_dg_col_Type span').innerText !== 'Folder' && row.querySelectorAll('img[src^="/d2l/img/0/LE_PT_ManageFiles.Main.infTopic.gif"]').length === 0);
	
		rows.forEach(row => {
			if(rows.length != checkedRows.length) {
				row.querySelector('input[type="checkbox"]').checked = true;
				row.classList.add('yui-dt-selected');
				row.querySelector('input[type="checkbox"]').dispatchEvent(new Event('click'));
			}
			else {				
				row.querySelector('input[type="checkbox"]').checked = false;
				row.classList.remove('yui-dt-selected');
				row.querySelector('input[type="checkbox"]').dispatchEvent(new Event('click'));
			}
		});
	});

})();