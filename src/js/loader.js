chrome.runtime.onMessage.addListener(function(request, sender, respond) {
	if(request.handshake == 'injectScript') {
		var ext = request.data.split('.');
		switch(ext[ext.length-1])
		{
			case 'css':
				if([...document.head.getElementsByTagName('link')].filter((link) => link.href == request.data).length == 0) {
					var css = document.createElement('link');
					css.type = 'text/css';
					css.rel = 'stylesheet';
					css.href = request.data;
					document.head.appendChild(css);
				}
				break;

			case 'js':
				if([...document.head.getElementsByTagName('script')].filter((script) => script.src == request.data).length == 0) {
					document.head.appendChild(document.createElement('script')).src=request.data;
				}
				break;
				
			default:
				console.error('Uncertain how to handle extension of type .%s', ext[ext.length-1]);
				break;
		}
	}	
});

