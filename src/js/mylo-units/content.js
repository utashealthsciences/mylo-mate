/** @namespace MyLO */
$(function() {  
	// Prevents the script from being run more than once.
	if(window.MyLOContentButtons) return;
	window.MyLOContentButtons = true;

	const _ = (r, all) => all ? document.querySelectorAll(r) : document.querySelector(r);

	// If we're viewing the unit as a student (Impersonating View Student), then hide all of our features.

	const isStudent = _('.d2l-list a').innerText != 'View as Student';
	if(isStudent) return;

	var id = _('a[href$="/Home"]').href.replace('/Home', '').split('/').pop();
	
	// Get some data from the storage area
	chrome.storage.sync.get({
		mylo: { adjustButtons: true, enhanceQuiz: true, enhanceAssessment: true }
	}, function(items) { 

		if ((items.mylo.enableUnitOutline || items.mylo.adjustButtons) && isDaylight()) {
			$('div.d2l-navigation-s-item:contains("Admin & Help"):first').before('<div class="d2l-navigation-s-item less-rpad" id="unit-links" role="list-item"></div>');
		}
		
		if(items.mylo.enableUnitOutline) {
			GetUnitData(id).then(({ directory }) => { 
				if (isDaylight()) {
					$('#unit-links').prepend(getPDFButtonCode(directory, {daylight: true}));
				} else {  
					$('div.AreaBoxBottomRight ul.d2l-navbar-area-list').prepend(getPDFButtonCode(directory)); 
				}
			
			}).catch(() => {/* There was no code. */});
		}

		// Add unit codes to the header of daylight.
		if(/*is enabled in options && */ isDaylight()) {
			GetUnitData(id).then(info => {
				//console.log(id, code);

				const { code } = info;

				const regex = /_([0-9]{2})(YR|S[0-9]{1})_/;
				const result = code.match(regex);

				const container = document.createElement('div');
				const title = _('.d2l-navigation-s-link');
				const subtitle = document.createElement('small');

				subtitle.style.fontSize = 'small';
				subtitle.style.display = 'block';
				subtitle.style.lineHeight = '0';
				subtitle.style.marginTop = '12px';
				subtitle.style.position = 'absolute';

				container.classList.add('mylo-mate-header-title');
				
				title.parentNode.insertBefore(container, title);
				title.style.fontSize = '1.25rem';

				container.appendChild(title);
				container.appendChild(subtitle);

				// Subtitle logic.

				let details = {
					type: '', 	// Award, non-award or sandpit
					year: '', 	// Year the unit ran
					format: '', // Semester or Term
					number: '',	// Sem 1, Term 2, etc.
					fullyear: false
				};

				if(code.startsWith('NA_SP_')) {
					subtitle.innerText = 'Sandpit';	
				}
				else {
					if(code.startsWith('AW_')) details.type = 'Award';
					else if(code.startsWith('NA_')) details.type = 'Non-Award';
	
					details.year = '20'+result[1];
	
					if(result[2].trim().toLowerCase() === 'yr') details.fullyear = true;
					else {
						details.format = result[2].startsWith('S') ? 'Semester' : 'Term';
						details.number = result[2].substring(1);
					}	
	
					subtitle.innerText = `${details.fullyear ? `${details.year} Full Year` : `${details.format} ${details.number} ${details.year}`}, ${details.type} Unit`;
				}
			}).catch(() => {/* There was no code. */ });
		}

		if (items.mylo.adjustButtons) { 
			if (isDaylight()) {
				var homeLink = $('a.d2l-navigation-s-link:contains("Content"):first').attr('href').split('/'); // Target Content link, as Unit Home link looks weird DEV2 31 July 
				var ou = homeLink[homeLink.length-2];
				$('#unit-links').append(getDLNavButtonCode(ou,'Manage Files','/d2l/lp/manageFiles/main.d2l?ou=','','/d2l/img/0/CMC.Main.actFiles.png?v=10.7.3.7486-189'));				
				$('#unit-links').append(getDLNavButtonCode(ou,'Unit Admin','/d2l/lp/cmc/main.d2l?ou=','','/d2l/img/0/CMC.Main.tbTools.png?v=10.7.3.7486-189')); 
			} else {  
				var homeLink = $('a.d2l-menuflyout-link-link:first').attr('href').split('/');
				var ou = homeLink[homeLink.length-1];		
				$('div.AreaBoxBottomRight ul.d2l-navbar-area-list a .d2l-image:last').remove();
				$('div.AreaBoxBottomRight ul.d2l-navbar-area-list').prepend(getNavButtonCode(ou,'Unit Admin','/d2l/lp/cmc/main.d2l?ou=','','/d2l/lp/navbars/6607/customlinks/icon/group/7668/2168163?v=10.7.1.6789-151'));	
				$('div.AreaBoxBottomRight ul.d2l-navbar-area-list').prepend(getNavButtonCode(ou,'Manage Files','/d2l/lp/manageFiles/main.d2l?ou=','','/d2l/img/0/CMC.Main.actFiles.png?v=10.7.1.6789-151'));	
			}			
		}
		if (items.mylo.enhancements) {
			var quizClass = (isDaylight()?'d2l-link':'vui-link');
			$('a.'+quizClass+'[title="Edit Quiz"]').each(function() {
				var uri = $(this).attr('href').split('?')[1], quizTitle = $(this).text();
				$(this).after(' <a class="'+quizClass+'" title="Submission Views for '+quizTitle+'" href="/d2l/lms/quizzing/admin/modify/quiz_newedit_subviews.d2l?'+uri+'">[s]</a> ');
				$(this).after(' <a class="'+quizClass+'" title="Assessment for '+quizTitle+'" href="/d2l/lms/quizzing/admin/modify/quiz_newedit_assessment.d2l?'+uri+'">[a]</a> ');
				$(this).after(' <a class="'+quizClass+'" title="Restrictions for '+quizTitle+'" href="/d2l/lms/quizzing/admin/modify/quiz_newedit_restrictions.d2l?'+uri+'">[r]</a> ');
			});
		
			var gradeClass = (isDaylight() ? 'd2l-link' : 'vui-link');
			let ou = (new URL(window.location.href)).searchParams.get('ou');
			$('a.' + gradeClass + '[onclick*="gotoNewEditItemProps"]').each(function () {
				var objID = $(this).attr('onclick').split('Props(')[1].split(');;')[0].trim(), gradeTitle = $(this).text();
				$(this).after(' <a class="' + gradeClass + '" title="Restrictions for ' + gradeTitle + '" href="/d2l/lms/grades/admin/manage/item_rests_edit.d2l?ou='+ ou +'&objectId='+objID+'&feedback=2">[r]</a> ');
			});
		
			var assClass = (isDaylight()?'d2l-link':'vui-link');
			$('a.'+assClass+'[href*="/d2l/lms/dropbox/admin/mark/folder_submissions_users.d2l"]').each(function() {
				var uri = $(this).attr('href').split('?')[1], assTitle = $(this).text();
				$(this).after(' <a class="'+assClass+'" title="Turnitin for '+assTitle+'" href="/d2l/lms/dropbox/admin/modify/folder_newedit_turnitin.d2l?'+uri+'">[t]</a> ');
				$(this).after(' <a class="'+assClass+'" title="Restrictions for '+assTitle+'" href="/d2l/lms/dropbox/admin/modify/folder_newedit_restrictions.d2l?'+uri+'">[r]</a> ');				
			});
		}
		if (items.mylo.overrideGrades) {
			CheckIfNotStudent(id).then(() => {
				// Not a student
				$('a[href*="/d2l/lms/grades/index.d2l"]')[0].href = '/d2l/lms/grades/admin/manage/gradeslist.d2l?' + $('a[href*="/d2l/lms/grades/index.d2l"]')[0].href.split('?')[1];
			}).catch(() => { /* Am student. */ });
		}
	});

	function GetPageSource(id) {
		return new Promise(function (resolve, reject) { $.get('https://mylo.utas.edu.au/d2l/home/' + id).done(function (data) { resolve(data); }); });
	}
	
	function GetUnitData(id) {
		return new Promise(function (resolve, reject) {
			chrome.storage.local.get(id, function (result) {
				if (Object.keys(result).length > 0) { resolve(result[id]); }
				else {
					GetPageSource(id).then(function (data) {
						let code = /var unitCode = "([^"]+)";/ig.exec(data);
						let directoryBlock = /d2lReplacements= {([^]+?)};/gi.exec(data);
						let d = {}; 
						
						if(code) {
							d[id] = d[id] || {};
							d[id]['code'] = code[1]; 
						}
						
						if(directoryBlock) {
							let directory = /OrgUnitPath[\s]*?:[\s]*?'([^]+?)',/gi.exec(directoryBlock[1]);

							d[id] = d[id] || {};
							d[id]['directory'] = directory[1]; 
						}

						chrome.storage.local.set(d);
						if(code || directoryBlock) resolve(d);
						else reject();
					});
				}
			});
		});
	}	

	function CheckIfNotStudent(id) {
		return new Promise(function (resolve, reject) {
			GetPageSource(id).then(function (data) {
				!data.includes(`RoleCode:'Student'`) ? resolve() : reject();
			});
		});
	}	
}); 

/**
 * Returns a pre-rendered HTML string for a button.
 * 
 * @param {string} ou The unit code number
 * @param {string} title The hover title of the icon
 * @param {string} lnk1 The link attached before the OU
 * @param {string} lnk2 The link attached after the OU
 * @param {string} ico The icon image link.
 * @returns {string} The final HTML string.
 * @memberof MyLO#
 */
function getNavButtonCode(ou,title,lnk1,lnk2,ico) {
	return '<li><div class="d2l-navbar-item"><a class="d2l-navbar-link" href="'+lnk1+ou+lnk2+'"><span style="width:16px; display:inline-block;"><img style="vertical-align:middle;" src="'+ico+'" title="'+title+'" alt="'+title+'"></span></a></div></li>';
}

/**
 * Returns a pre-rendered HTML string for a button to the Unit Outline.
 * 
 * @param {string} ou The unit code number
 * @param {string} path The unit code ID
 * @returns {string} The final HTML string.
 * @memberof MyLO#
 */
function getPDFButtonCode(path,{daylight=false}={}) {
	return daylight
		? '<div class="d2l-navigation-s-item less-rpad" role="listitem"><a class="d2l-navigation-s-link" href="'+ path +'UnitOutline/UnitOutline.pdf"><span style="width:16px; display:inline-block;"><img style="vertical-align:middle;width:20px;" src="/shared/Images/Info_widget/pdflogo.jpg" title="Unit Outline" alt="Unit Outline"></span></a></div></li>' 
		: '<li><div class="d2l-navbar-item"><a class="d2l-navbar-link" href="'+ path +'UnitOutline/UnitOutline.pdf" target="_blank"><span style="width:16px; display:inline-block;"><img style="vertical-align:middle;width:20px;" src="/shared/Images/Info_widget/pdflogo.jpg" title="Unit Outline" alt="Unit Outline"></span></a></div></li>';
}



/**
 * Returns a pre-rendered HTML string for a button, for use in the Daylight theme.
 * 
 * @param {string} ou The unit code number
 * @param {string} title The hover title of the icon
 * @param {string} lnk1 The link attached before the OU
 * @param {string} lnk2 The link attached after the OU
 * @param {string} ico The icon image link.
 * @returns {string} The final HTML string.
 * @memberof MyLO#
 */
function getDLNavButtonCode(ou,title,lnk1,lnk2,ico) {
	return '<div class="d2l-navigation-s-item less-rpad" role="listitem"><a class="d2l-navigation-s-link" href="'+lnk1+ou+lnk2+'"><span style="width:16px; display:inline-block;"><img style="vertical-align:middle;" src="'+ico+'" title="'+title+'" alt="'+title+'"></span></a></div>';
}

/**
 * Returns if the template is currently using Daylight.
 * @returns {boolean} Returns `true` if the body contains the `daylight` class.
 * @memberof MyLO#
 */
function isDaylight() {
	return document.body.classList.contains('daylight');
}


