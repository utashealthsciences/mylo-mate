(function() {
	if(window.FixBulkDeleteCheckboxes) return;
	window.FixBulkDeleteCheckboxes = true;

	document.querySelector('.d2l-selectall input[type=checkbox]').addEventListener('click', (e) => {
		if(e.target.checked) document.querySelector('.d2l-datalist-item input[type=checkbox]').dispatchEvent(new Event('click'));
	});
})();