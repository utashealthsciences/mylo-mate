(function() {
	if(window.DiscussionShortcutsAdded) return;
	window.DiscussionShortcutsAdded = true;

	/*
		URL to Restrictions:
			https://mylo.utas.edu.au/d2l/lms/discussions/admin/forum_topic_edit_restrictions.d2l?ou=190777&id=257477&type=t&actId=8

		URL to Assessment:
			https://mylo.utas.edu.au/d2l/lms/discussions/admin/topic_new_scoring.d2l?ou=190777&tid=257477&actId=7&d2l_isfromtab=1
	*/

	const isStudent = document.querySelector('.d2l-list a').innerText != 'View as Student';
	if(isStudent) return;

	let ou = window.location.href.split('/discussions/')[0].split('/').pop();
	document.querySelector('div[id^="TopicList_For_"]').querySelectorAll('th[scope="row"] > div:first-child').forEach(row => {
		let topicID = row.querySelector('div').attributes[0].value;
		let heading = row.querySelector('h3');

		let restrictions = document.createElement('a');
		restrictions.href = `/d2l/lms/discussions/admin/forum_topic_edit_restrictions.d2l?ou=${ou}&id=${topicID}&type=t&actId=8`;
		restrictions.innerText = '[r]';		
		restrictions.classList.add('vui-link');

		let assess = document.createElement('a');
		assess.href = `/d2l/lms/discussions/admin/topic_new_scoring.d2l?ou=${ou}&tid=${topicID}&actId=7`;
		assess.innerText = '[a]';
		assess.classList.add('vui-link');

		// console.log(ou, topicID, heading);
		heading.appendChild(restrictions);
		heading.appendChild(assess);
	});

})();