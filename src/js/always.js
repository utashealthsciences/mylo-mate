// This is loaded into every page that the extension matches against.
// Be extremely careful not to abuse this.

(function() {
	if(window.LoadAlways) return;
	window.LoadAlways = true;

	document.querySelectorAll('.fa-help').forEach(help => { 
		help.addEventListener('click', (e) => {
			chrome.extension === undefined
				? alert(e.target.title)
				: chrome.extension.getBackgroundPage().alert(e.target.title);
		});
	});
})();
