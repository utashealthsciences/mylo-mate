/*global _*/
/**********************************
 * 
 * 			  Shortcuts
 * 
 **********************************/

// Only run if user groups enabled.
function ready () {
	// Window guard
	// Prevents the script from being run more than once.
	if (window.UsersRun) return;
	window.UsersRun = true;

	if (window._ === undefined) window._ = (...rest) => document.querySelector(rest);

	/**********************************
	 * 
	 * 			   Dataset
	 * 
	 **********************************/

	/************************
	 * 
	 * 	  Data structures
	 * 
	 ************************/

	/**
	 * Generate a new User.
	 * 
	 * @param {string} name The users preferred name
	 * @param {string} surname The users surname
	 * @param {string} id The users Staff ID
	 * @param {string} user The users username
	 * @param {string} schools The schools that the user is a part of
	 * @param {string} role The users default role
	 * @returns {Types#User} Returns a new User object.
	 * @memberof MyLO Manager/Users#
	 */
	function User({ PreferredName, Surname, StaffID, Username, Schools, Role }) {
		this.StaffID = StaffID;
		this.Username = Username;
		this.PreferredName = PreferredName;
		this.Surname = Surname;
		this.Schools = Schools;
		this.Role = Role;
	}

	/**
	 * Generate a new Group.
	 * 
	 * @param 	{string} 		id 		The internal ID of the group
	 * @param 	{string} 		name 	The display name of the group.
	 * @prop 	{Types#User[]} 	Users	The Users in the Group.
	 * @memberof MyLO Manager/Users#
	 */
	function Group(id, name) {
		this.ID = id;
		this.Name = name;
		this.Users = [];
	}

	/************************
	 * 
	 * 	   Functionality
	 * 
	 ************************/


	/**
	 * Groups parent object.
	 * @prop	{function}	create	Used to create a new Group.
	 * @prop	{function}	get		Retrieves a group from the listing, based on the ID passed. If no ID is provided, it will return all groups.
	 * @prop	{function}	update	Updates a groups name, based on the ID.
	 * @prop	{function}	delete	Deletes a group, based on the ID.
	 * @memberof MyLO Manager/Users#
	 */
	const Groups = {
		create(id, name) {
			return new Promise(resolve => {
				if (name.trim().length > 0) {
					Dataset.push({
						[id]: new Group(id, name.trim())
					}).then(() => resolve(id, name.trim()));
				}
			});
		},
		get(id = null) {
			return new Promise(resolve => {
				if (id === -1) { /* reject('Invalid group'); */ }
				else {
					Dataset.pull().then((data) => {
						if (id !== null) resolve(data[id]);
						else resolve(data);
					});
				}
			});

		},
		update(obj) {
			return new Promise(resolve => {
				this.get(obj.ID).then((group) => {
					Dataset.push({ [obj.ID]: $.extend({}, group, obj) }).then(resolve);
				});
			});
		},
		delete(id) {
			this.get(null).then((groups) => {
				Dataset.push(groups.filter((group) => group.id !== id));
			});
		},
		sanitize(input) {
			return input.toLowerCase().replace(/[^\w\s\d]/gi, '').replace(/[\s]/g, '-');
		},
		user: {
			add(group, user) {
				const match = group.Users.filter((u) => u.StaffID === user.StaffID);
				const remaining = group.Users.filter((u) => u.StaffID !== user.StaffID);
				if (match.length === 0) {
					group.Users.push(new User(user));
				}
				else if (match.length === 1 && match.Role !== user.Role) {
					group.Users = Array.prototype.concat(remaining, new User(user));
				}

				return Groups.update(group);
			}
		}
	};

	const Dataset = {
		/**
		 * Pulls from the chrome storage area.
		 * 
		 * @returns {Promise} A promise that resolves with the dataset.
		 */
		pull() {
			return new Promise((resolve) => {
				const search = { 'usergroups': null };
				chrome.storage.sync.get(search, (data) => {
					resolve(data.usergroups.groups);
				});
			});
		},

		/**
		 * Pushes to the chrome storage area.
		 * 
		 * @param {object} dataset The dataset to submit.
		 * @returns {Promise} A promise that resolves when the data has been pushed.
		 */
		push(dataset) {
			return new Promise((resolve) => {
				chrome.storage.sync.get({ 'usergroups': null }, (source) => {
					chrome.storage.sync.set({
						'usergroups': {
							'enabled': source.usergroups.enabled,
							'groups': $.extend({}, source.usergroups.groups, dataset)
						}
					}, resolve);
				});
			});
		}
	};

	/**********************************
	 * 
	 * 	  		End dataset
	 * 
	 **********************************/



	/**********************************
	 * 
	 * 		   		 UI
	 * 
	 **********************************/

	const UI = {
		get StaffSearchGroup() {
			return _('#usergroups').value === 'default' ? -1 : _('#usergroups').value;
		},
		get QuickGroup() {
			return _('#quick-usergroups').value === 'default' ? -1 : _('#quick-usergroups').value;
		},
		get container() {
			return _('#staffSearchDialog').parentNode;
		},
		UpdateGroupsList(select, { selected, removeDefault = false } = {}) {
			return new Promise(resolve => { 
				Groups.get(null).then((groups) => {
					// console.log(groups);
					if (groups === null) return;

					// Loops through each option already in the list,
					// and removes it if it isn't the default 'Select a group..' entry.
					for (let i = select.options.length - 1; i >= 0; i--) {
						if (removeDefault || !select.options[i].attributes.default) {
							select.options.remove(i);
						}
					}

					for (const id in groups) {
						if (id.startsWith('_')) continue;
						const e = groups[id];
						// console.log(e, groups);
						let opt = document.createElement('option');
						opt.innerHTML = e.Name; opt.value = e.ID;
						if (id === selected) { opt.selected = true; }
						select.appendChild(opt);
					}

					resolve();
				});
			});
		},
		prompt(title, desc, placeholder) {
			return new Promise((resolve, reject) => {
				let dialog = document.createElement('dialog');

				let name = document.createElement('p');
				name.className = 'name';
				name.innerText = title || '';

				let info = document.createElement('div');
				info.className = 'info';
				info.innerHTML = desc || '';

				let input = document.createElement('input');
				input.type = 'text';
				input.placeholder = placeholder || '';

				input.addEventListener('keyup', (e) => { if (e.keyCode === 13) { dialog.close(); resolve(input.value); } });

				let dialog_yes = document.createElement('button');
				dialog_yes.innerText = 'OK';
				dialog_yes.classList.add('btn');
				dialog_yes.classList.add('okay_btn');
				dialog_yes.addEventListener('click', function () { dialog.close(); resolve(input.value); });

				let dialog_no = document.createElement('button');
				dialog_no.innerText = 'Cancel';
				dialog_no.classList.add('btn');
				dialog_no.classList.add('cancel_btn');
				dialog_no.addEventListener('click', function () { dialog.close(); reject(); });

				dialog.appendChild(name);
				dialog.appendChild(info);

				dialog.appendChild(input);

				dialog.appendChild(dialog_yes);
				dialog.appendChild(dialog_no);

				document.body.appendChild(dialog);

				dialog.showModal();
			});

		},
		alert(title, desc) {
			let dialog = document.createElement('dialog');

			let name = document.createElement('p');
			name.className = 'name';
			name.innerText = title || '';

			let info = document.createElement('div');
			info.className = 'info';
			info.innerHTML = desc || '';

			let okay = document.createElement('button');
			okay.innerText = 'OK';
			okay.classList.add('btn');
			okay.classList.add('okay_btn');
			okay.addEventListener('click', function () { dialog.close(); });

			dialog.appendChild(name);
			dialog.appendChild(info);
			dialog.appendChild(okay);

			document.body.appendChild(dialog);

			dialog.showModal();
		},
		notify(title, info, { parent = this.container, timeout=5, classes='center'}={}) {
			let n = document.createElement('div');
			n.className = 'visible ' + classes; n.id = 'notify';
			n.innerHTML = `<p class="title">${title}<span class="close" title="Dismiss message">x</span></p><p class="desc">${info}</p>`;
			parent.appendChild(n);

			let timer = setTimeout(() => n.classList.add('fade'), timeout * 1000);
			n.querySelector('.close').addEventListener('click', () => { clearTimeout(timer); parent.removeChild(n); });
			n.addEventListener('animationend', () => { parent.removeChild(n); });		
		},
		custom: {
			promptUserAdd(title, desc, placeholder, defaultValue, roleText) {
				return new Promise((resolve, reject) => {
					let dialog = document.createElement('dialog');
					dialog.classList.add('dialog');
					dialog.classList.add('dialog-prompt');


					let name = document.createElement('p');
					name.className = 'name';
					name.innerText = title || '';

					let info = document.createElement('div');
					info.className = 'info';
					info.innerHTML = desc || '';

					let input = document.createElement('input');
					input.type = 'text';
					input.placeholder = placeholder || '';
					input.value = defaultValue || '';

					input.addEventListener('keyup', (e) => { if (e.keyCode === 13) { dialog.close(); resolve({ users: input.value, role: roles.value }); document.body.removeChild(dialog); } });

					let role = document.createElement('p');
					role.innerHTML = roleText;
					role.className = 'roleText';

					const UserRoles = ['Auditor', 'Lecturer', 'Student', 'Support', 'Tutor'];

					let roles = document.createElement('select');
					roles.style.display = 'inline-block';
					roles.innerHTML = UserRoles.map(role => `<option value="${role}"${role === 'Support' ? ' selected' : ''}>${role}</option>`).join('\n');

					role.appendChild(roles);

					let dialog_yes = document.createElement('button');
					dialog_yes.innerText = 'OK';
					dialog_yes.classList.add('btn');
					dialog_yes.classList.add('okay_btn');
					dialog_yes.addEventListener('click', function () { dialog.close(); resolve({ users: input.value, role: roles.value }); document.body.removeChild(dialog); });

					let dialog_no = document.createElement('button');
					dialog_no.innerText = 'Cancel';
					dialog_no.classList.add('btn');
					dialog_no.classList.add('cancel_btn');
					dialog_no.addEventListener('click', function () { dialog.close(); reject(); document.body.removeChild(dialog); });

					dialog.appendChild(name);
					dialog.appendChild(info);

					dialog.appendChild(input);

					dialog.appendChild(role);

					dialog.appendChild(dialog_yes);
					dialog.appendChild(dialog_no);


					document.body.appendChild(dialog);

					dialog.showModal();
				});
			},
			search() {
				return new Promise((resolve, reject) => {
					const req = ({username='', given='', surname='', staffid='', dept=''}) => new Promise((resolve, reject) => { 
						$.ajax({
							url: `https://mylo-manager.utas.edu.au/User/StaffSearch?Surname=${surname}&GivenName=${given}&Username=${username}&DepartmentCode=${dept}&StaffId=${staffid}`,
							dataType: 'json',
							type: 'POST',
							data: { rows: 500, sidx: 'givenname' },
							success: resolve,
							error: reject
						});
					});
					
					const AddRows = (data) => {
						if (data === undefined || data.length == 0) {
							// This might not be the best method, given that it just hides the table if there's no results.
							// It would probably be better to have a "no results" text box, but work on that later.
							results.classList.add('hidden');
							loader.classList.add('hidden');
							return;
						}

						const body = results.querySelector('tbody');
						_('#select-all').checked = data.length === 1;
						data.forEach((row) => {
							const [ given, last, user, id, school ] = row.cell;
							const r = document.createElement('tr');
							r.innerHTML = `
							<td>${given}</td>
							<td>${last}</td>
							<td>${user}</td>
							<td>${id}</td>
							<td width="20px"><input type="checkbox"${data.length === 1 ? ' checked ' : ''}class="user-select" data-source="${encodeURIComponent(JSON.stringify({ PreferredName: given, Surname: last, Username: user, StaffID: id, Schools: school }))}" name="${user}"></td>
							`;
							body.appendChild(r);
						});

						document.querySelectorAll('.user-select').forEach(cb => cb.addEventListener('click', () => {
							// If all user-select checkboxes have been checked, then check the select all button.
							// If there are any that remain unchecked (or have been unchecked), then uncheck the select all button.
							_('#select-all').checked = document.querySelectorAll('.user-select[type="checkbox"]:not(:checked)').length == 0;
						}));
						
						loader.classList.add('hidden');
						results.classList.remove('hidden');
						if (data.length > 10) {
							// We have a scrollbar.
							results.classList.add('scroll');
							results.classList.remove('full');
						}
						else {
							// No scrollbar
							results.classList.remove('scroll');
							results.classList.add('full');
						}
					};

					const empty = (...fields) => fields.filter(f => f.value.trim().length > 0).length == 0;

					if(!Array.prototype.unique) {
						Array.prototype.unique = function() {
							let seen = {};
							return this.filter(item => seen.hasOwnProperty(item) ? false : (seen[item]=true));
						};
					}

					const search = () => {
						results.querySelector('tbody').innerHTML = '';
						results.classList.remove('full');
						results.classList.remove('scroll');
						if (empty(username, given_name, surname, staffID, dept)) {
							
							results.classList.add('hidden');
							return;
						}
						if(username.value.includes(',') && empty(given_name, surname, staffID, dept)) {

							loader.classList.remove('hidden');

							// Only the username field is set, and it contains commas
							let stack = username.value.split(',').filter(u => u.trim().length > 0).unique().map(u => req({ username: u.trim() }));
							Promise.all(stack).then(res => {
								let users = res.filter(r => r.records > 0).map(r => r.rows[0]);
								AddRows(users);
							});
						}
						else {
							loader.classList.remove('hidden');
							req({
								username: username.value, 
								given: given_name.value, 
								surname: surname.value, 
								staffid: staffID.value, 
								dept: dept.value
							}).then(data => AddRows(data.rows));
						}
					};

					const close = () => {
						dialog.close();
						document.body.removeChild(dialog);
					};
					
					let dialog = document.createElement('dialog');
					dialog.classList.add('dialog');
					dialog.classList.add('dialog-prompt');
					dialog.classList.add('dialog-search');

					let wrapper = document.createElement('div');
					wrapper.className = 'wrapper';
					
					let title = document.createElement('legend');
					title.innerText = 'Search for User';
					wrapper.appendChild(title);

					let description = document.createElement('p');
					description.className = 'description';
					description.innerText = '';

					let username = document.createElement('input');
					username.className = 'search-input';
					username.type = 'text';
					username.placeholder = 'Username';
					username.value = '';

					let given_name = document.createElement('input');
					given_name.className = 'search-input';
					given_name.type = 'text';
					given_name.placeholder = 'Given name';
					given_name.value = '';

					let surname = document.createElement('input');
					surname.className = 'search-input';
					surname.type = 'text';
					surname.placeholder = 'Surname';
					surname.value = '';

					let staffID = document.createElement('input');
					staffID.className = 'search-input';
					staffID.type = 'text';
					staffID.placeholder = 'Staff ID';
					staffID.value = '';

					let dept = document.createElement('select');
					dept.className = 'search-select';
					dept.innerHTML = _('#StaffSearchDepartment').innerHTML;
					var maxLen = 80;
					dept.querySelectorAll('option').forEach(o => {
						o.title = o.innerText;
						if (o.innerText.length > maxLen) {
							var str = o.innerText.substr(0, (maxLen / 2) - 3);
							str += '...';
							str += o.innerText.substr((maxLen / 2) * -1);
							o.innerText = str;
						}
					});

					username.addEventListener('keyup', (e) => { if (e.keyCode === 13) { search(); } });
					given_name.addEventListener('keyup', (e) => { if (e.keyCode === 13) { search(); } });
					surname.addEventListener('keyup', (e) => { if (e.keyCode === 13) { search(); } });
					staffID.addEventListener('keyup', (e) => { if (e.keyCode === 13) { search(); } });

					const searchButton = document.createElement('button');
					searchButton.id = 'search_button';
					searchButton.className = 'btn';
					searchButton.innerText = 'Search';
					searchButton.addEventListener('click', search);			


					let results = document.createElement('table');
					results.classList.add('hidden');
					results.innerHTML = `
					<thead>
						<th>Given Name</th>
						<th>Surname</th>
						<th>Username</th>
						<th>Staff ID</th>
						<th width="20px"><input type="checkbox" title="Select All" id="select-all"></th>
					</thead>
					<tbody>
					</tbody>
					`;

					let loader = document.createElement('div');
					loader.classList.add('loader');
					loader.classList.add('hidden');

					let role = document.createElement('p');
					role.innerHTML = 'Select the role for these users: ';
					role.className = 'roleText';

					const UserRoles = ['Auditor', 'Lecturer', 'Student', 'Support', 'Tutor'];

					let roles = document.createElement('select');
					roles.style.display = 'inline-block';
					roles.innerHTML = UserRoles.map(role => `<option value="${role}"${role === 'Support' ? ' selected' : ''}>${role}</option>`).join('\n');

					role.appendChild(roles);

					let add = document.createElement('button');
					add.innerText = 'Add';
					add.classList.add('btn');
					add.classList.add('okay_btn');
					add.addEventListener('click', function () { 
						let users = [];
						results.querySelectorAll('.user-select[type="checkbox"]:checked').forEach(row => users.push({ ...JSON.parse(decodeURIComponent(row.dataset.source)), Role: roles.value } ));
						close();
						resolve(users); 
					});

					let closeBtn = document.createElement('button');
					closeBtn.innerText = 'Close';
					closeBtn.classList.add('btn');
					closeBtn.addEventListener('click', () => {
						close();
						reject();
					});
					
					dialog.appendChild(description);
			
					wrapper.appendChild(username);
					wrapper.appendChild(staffID);
					wrapper.appendChild(given_name);
					wrapper.appendChild(surname);
					wrapper.appendChild(dept);
					wrapper.appendChild(searchButton);
					dialog.appendChild(wrapper);

					dialog.appendChild(results);
					dialog.appendChild(loader);
					dialog.appendChild(role);				
					dialog.appendChild(add);	
					dialog.appendChild(closeBtn);	

					document.body.appendChild(dialog);

					dialog.querySelector('#select-all').addEventListener('click', e => {
						dialog.querySelectorAll('.user-select[type="checkbox"]').forEach(cb => cb.checked = e.target.checked);
					});

					dialog.showModal();
				});
			
			}
		}
	};

	const Grid = {
		get elem() { return _('#grdDialogResults'); },
		get() {
			return new Promise((resolve) => {
				let ts = new Date().getTime();
				const handler = (e) => {
					if (e.detail.ts === ts) { document.removeEventListener('USERGROUPS_RPC', handler); }
					resolve(e.detail.payload);
				};
				document.addEventListener('USERGROUPS_RPC', handler);
				document.dispatchEvent(new CustomEvent('USERCONTENT_RPC', { detail: { flag: 'GET', ts } }));
			});
		},
		get selectedUsers() {
			return new Promise((resolve) => {
				this.get().then((users) => resolve(users.filter((user) => user.checked)));
			});
		},
		add(users) {
			document.dispatchEvent(new CustomEvent('USERCONTENT_RPC', { detail: { flag: 'SET', payload: users } }));
		}
	};

	const QuickUse = {
		add(users) {
			const UnitID = window.location.pathname.split('/').pop();
			let stack = [];
			users.forEach((user) => stack.push(new Promise(resolve => {
				const matching = (username) => {
					const rows = document.querySelectorAll('#grdStaff tbody tr');
					for (let i = 0; i < rows.length; i++) {
						if (rows[i].querySelector('td input').value == username) return true;
					}
					return false;
				};
				
				$.post('/UnitRequest/AddStaff/' + UnitID, {
					StaffID: user.StaffID,
					PreferredName: user.PreferredName,
					Surname: user.Surname,
					Username: user.Username,
					Department: user.Schools
				},
				function (data) {
					if (matching(user.Username)) $('#grdStaff tr:has(input[value="'+user.StaffID+'"])').remove();
					let d = document.createElement('tr'); d.innerHTML = data;
					d.querySelector(`select[name$=".MyLORole"] option[value="${user.Role || 'Support'}"]`).selected = true; 
					$('#grdStaff tbody').append(d); 				
				}).always(() => resolve());				 			
			})));
		
			return Promise.all(stack);
		}
	};

	const GroupDialog = _('#staffSearchDialog').cloneNode();
	GroupDialog.id = 'staffGroupDialog';
	_('#staffSearchDialog').parentNode.appendChild(GroupDialog);

	_('#staffGroupDialog').appendChild((() => {
		let field = document.createElement('div');
		field.innerHTML = `
			<p>Add, edit or remove groups using the icons to the right.</p>
			<div class="actions" id="group-actions">
				<span class="icon icon-add add action" title="Add"></span>
				<span class="icon icon-edit edit action" title="Edit"></span>
				<span class="icon icon-delete delete action" title="Delete"></span>
			</div>
			<select class="usergroups-select" id="usergroups_groups"></select> 
			<p>Select users to modify their roles or remove them from the group.</p>
			<div class="actions" id="user-actions">
				<span class="icon icon-add add action" title="Add"></span>
				<span class="icon icon-edit edit action" title="Edit"></span>
				<span class="icon icon-delete delete action" title="Delete"></span>
			</div>
			<select class="usergroups-select" id="usergroups_users" multiple></select>
			<button id="closeGroupManager" type="button" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" role="button" aria-disabled="false">
				<span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span>
				<span class="ui-button-text">Close</span>
			</button>
			`;
		return field;
	})());
	_('#closeGroupManager').addEventListener('click', () => $('#staffGroupDialog').dialog('close'));
	$('#staffGroupDialog').dialog({ autoOpen: false, height: 390, width: 500, modal: true, title: 'Manage groups' });

	const addUser = () => {
		UI.custom.search().then((users) => {
			// Get our groups
			Groups.get().then((groups) => {
				// Pull out our current group.
				const current = groups[_('#usergroups_groups').value];

				// Get the usernames from our add panel.
				let newUsers = users.map(u => u.Username);

				// Get our list of existing usernames
				let existingUsers = current.Users.map(u => u.Username);

				// Builds a list of promises, by finding usernames that aren't part of the list yet
				//	and then doing a data request to the server.
				let u = newUsers
					.filter(user => !existingUsers.includes(user))
					.map(user => users.filter(usr => user == usr.Username)[0])
					.map(user => new User(user));

				if (u.length > 0) {
					Array.prototype.push.apply(current.Users, u);
					PushGroups(groups).then(() => {
						UpdateUsers(current.Users);					
						UI.notify('Success', `Successfully added: ${u.map(u => `'${u.Username}'`).join(',')}`, { timeout: 2, classes: 'center', parent: _('#staffGroupDialog')});
					});
				}
			});
		}).catch(() => {}); // Dialog was closed.
	};

	chrome.storage.sync.get(null, data => document.dispatchEvent(new CustomEvent('Ready', { detail: { data, events: { user: { add: addUser } }, parent: _('#staffGroupDialog') } })));

	let quickgroups = document.createElement('fieldset');
	Groups.get().then(groups => new Promise(resolve => {
		quickgroups.innerHTML = `<legend>Add Predefined Group</legend>
				<p class="${Object.keys(groups).length > 0 ? 'hidden' : ''}">To create a predefined group, either use the Add Staff Member - Search window, or use the <a href="extension://options">Quicklinks Options</a>.</p>
				<table class="Borderless">
					<tbody>
					<tr>
						<td colspan="2">
							Select a group
						</td>
					</tr>
					<tr>
						<td>
							<select name="quick-usergroups" id="quick-usergroups" style="min-width: 20em;">
								<option style="color:grey;" default value="default">Select a group..</option>
							</select>
						</td>
						<td align="right">
							<a class="fm-button-icon-left ui-corner-all ui-state-default fm-button" id="btnStaffGroupAdd"><span class="ui-icon-arrowthick-1-n ui-icon"></span>Add Group to Unit</a>
							<a class="fm-button-icon-left ui-corner-all ui-state-default fm-button" id="btnStaffUsersAdd"><span class="ui-icon-arrowthick-1-n ui-icon"></span>Add Username List to Unit...</a>
							<a class="fm-button-icon-left ui-corner-all ui-state-default fm-button" id="btnGroupsEdit"><span class="fa-icon fa-cogs"></span>Manage Groups</a>
						</td>
					</tr>
				</tbody></table>`;
		_('fieldset.UnitRequestTabPage').append(quickgroups);
		resolve();	
	})).then(() => {
		document.querySelectorAll('a[href^="extension://"]').forEach(a => {
			a.addEventListener('click', e => {
				e.preventDefault();
				if (e.target.href.endsWith('options')) {
					chrome.runtime.sendMessage({open: 'options'});
				}
			});
		});
		
		_('#btnStaffGroupAdd').addEventListener('click', () => {
			Groups.get(UI.QuickGroup).then((group) => {
				QuickUse.add(group.Users).then(() => {
					UI.notify('Success', 'Users added successfully.', { timeout: 2, classes: 'center', parent: _('.UnitRequestTabPage') });
				});
			});
		});

		_('#btnStaffUsersAdd').addEventListener('click', () => {
			UI.custom.promptUserAdd('Add Users', 'Insert a list of usernames to add, seperated by a comma.<br>Example: <pre>username1, username2, username3</pre>', 'Usernames to add', '', 'Role for these users: ').then(({ users, role }) => {

				// Break the usernames list into single usernames
				const usernames = users.split(',').filter(u => u.trim().length > 0).map((u) => u.trim());

				const existing = [...document.querySelectorAll('input[name$=".UserName"]')].map(u => u.value);

				let stack = usernames.filter(u => existing.indexOf(u) === -1).map(username => new Promise((resolve, reject) => {
					$.ajax({
						url: 'https://mylo-manager.utas.edu.au/User/StaffSearch?Surname=&GivenName=&Username=' + username + '&DepartmentCode=&StaffId=',
						dataType: 'json',
						type: 'POST',
						data: {
							sidx: 'givenname',
						},
						success: (data) => {
							// NOTE:
							// 		ALL STAFF DETAILS SHOULD BE RETREIVED BY STAFF USERNAME ONLY.
							//		THIS IS TO REMOVE THE RISK OF DUPLICATE RECORDS!
							if (data.total === 0) { resolve({ success: false, username }); return; }
					
							data = data.rows.filter((row) => row.id == username)[0];
					
							if (data === undefined) { resolve({ success: false, username }); return; }

							resolve({
								id: data.id,
								PreferredName: data.cell[0],
								Surname: data.cell[1],
								Username: data.cell[2],
								StaffID: data.cell[3],
								Schools: data.cell[4],
								success: true
							});
						},
						error: reject
					});
				}));
			
				Promise.all(stack).then((userData) => {
					let success = []; let errors = [];

					const users = userData
						.filter(u => { u.success ? success.push(u.Username) : errors.push(u.username); return u.success; })
						.map(u => new User({ ...u, Role: role }));

					if (users.length > 0) {
						QuickUse.add(users).then(() => {
							let messages = [];
							if (success.length > 0) messages.push(`Successfully added: ${success.map(u => `'${u}'`).join(',')}`);
							if (errors.length > 0) messages.push(`Failed to add: ${errors.map(u => `'${u}'`).join(',')}`);

							let title = 'Notice';
							if (success.length == 0) title = 'Error';
							if (errors.length == 0) title = 'Success';

							UI.notify(title, messages.join('<br>'), {timeout: 2, classes: 'center', parent: _('.UnitRequestTabPage')});
						});
					} else if (errors.length > 0) {
						UI.notify('Error', 'Failed to add users: ' + errors.join(','), {timeout: 2, classes: 'center', parent: _('.UnitRequestTabPage')});
					}

				}).catch((err) => {
					// Something went wrong when we were collecting the data from the server!
					UI.notify('Error', 'There was an error retrieving the data from the server. Please see the console for more information.', { classes: 'center', parent: _('.UnitRequestTabPage') });
					console.error(err);
				});
			}).catch(() => {/* User cancelled the prompt. */ });
		});
		
		_('#btnGroupsEdit').addEventListener('click', () => {
			$('#staffGroupDialog').dialog('open');
		});

		UI.UpdateGroupsList(_('#quick-usergroups'), { removeDefault: true });
	
		document.addEventListener('Group.Modify', () => UI.UpdateGroupsList(_('#quick-usergroups'), { removeDefault: true }));

	});
}

// Runs no matter the setting.
$(function() {
	// Runs the search action when enter is pressed in these fields.	
	const search = (e) => { if (e.keyCode === 13) { document.querySelector('#btnDialogSearch').dispatchEvent(new Event('click')); } };

	document.querySelector('#txtDialogUserName').addEventListener('keyup', search);
	document.querySelector('#txtDialogStaffId').addEventListener('keyup', search);
	document.querySelector('#txtDialogSurname').addEventListener('keyup', search);
	document.querySelector('#txtDialogGivenName').addEventListener('keyup', search);

	document.querySelector('#staffSearchDialog').parentNode.id="staffSearchDialogContainer";

	document.addEventListener('GridReady', () => {

		if (document.querySelectorAll('#select-all').length === 0) {
			const header = document.querySelector('#jqgh_grdDialogResults_Select');
			header.innerHTML = '<input type="checkbox" id="select-all"> ' + (header.innerHTML.replace('Select', ''));
		}

		document.querySelector('#select-all').addEventListener('click', (e) => {
			e.stopPropagation ? e.stopPropagation() : e.cancelBubble = true;
			document.querySelector('#grdDialogResults').querySelectorAll('tr:not(:first-child)').forEach((row) => row.querySelector('input[type="checkbox"]').checked = e.target.checked);
		});

		document.querySelector('#grdDialogResults').querySelectorAll('tr:not(:first-child) input[type="checkbox"]').forEach((checkbox) => {
			checkbox.addEventListener('click', (e) => {
				e.stopPropagation ? e.stopPropagation() : e.cancelBubble = true;
				if (!e.target.checked) document.querySelector('#select-all').checked = false;
			});
		});
	});

	chrome.storage.sync.get({ 'usergroups': null }, (d) => d.usergroups.enabled ? ready() : null);

});