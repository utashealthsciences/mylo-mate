/* global store extensionID */ // This is used for ESLint. It just stops errors about the variable not being defined.
/** @namespace MyLO Manager */
// If the role is matches an entry in this array, then enable quicklinks. Otherwise, fade the bar out.
var allowedRoles = ['Support','Lecturer','UnitCoordinator','Auditor','Tutor'];

// Handler for the Bulk User Management (BUM) Tool dialog panel.
var dialog;

/**
 * Adds the quicklinks to each row of MyLO Manager.
 * @memberof MyLO Manager#
 */
function addQuicklinks() {
	// Remove any old quicklink DIVs
	$('.quicklink').remove(); 

	if (store.manager.enablePending) { 
		// If 'Enable Pending' is true, then it will re-enable the unit link for pending units.
		//		(because mylo manager disables it for some strange reason?)
		$('td[aria-describedby="grdMyUnits_Status"').each(function() { 
			var o = $(this);
			if (o.prop('title').startsWith('Pending')) { // Loop through only those rows with a Pending status 
				var ancestor = o.parent(); // parent TR tag
				var titleTD = $('td[aria-describedby="grdMyUnits_UnitTitle"]',ancestor);
				if (!titleTD.has('a').length) { // Link has been stripped, so add it back in
					titleTD.html('<a href="https://mylo.utas.edu.au/d2l/lp/homepage/home.d2l?ou='+$('td[aria-describedby="grdMyUnits_OrgUnitID"]',ancestor).prop('title')+'" target="_blank">'+titleTD.prop('title')+'</a>');
				}
				o.prop('title','Pending...').text('Pending...');
			}
		}); 
	}

	if (store.manager.addQuicklinks) {
		// If 'Add Quicklinks' is true, then it will go through each row and add the quicklinks panel to each item.
		$('td[aria-describedby="grdMyUnits_UnitTitle"] a').each(function() { // Iterate each unit title in the table
			var o = $(this);
			var ancestor = o.parent(); // parent TD tag
			var role = ancestor.siblings('[aria-describedby="grdMyUnits_MyRole"]').prop('title'); // retrieve unit role e.g. Support, Student, etc
			var href = o.attr('href');
			if (href.indexOf('=')>0) { // Only consider hrefs that contain a unit ID (realistically, that's all of them)
				var ou = href.split('=')[1];
				if(ou) { // Tests if value is 'falsey'; e.g. undefined, null, empty string, etc.
					var outHTML = '<div class="quicklink'+($.inArray(role,allowedRoles)>=0?'':' faded')+'">' + getLink('https://mylo.utas.edu.au/d2l/le/content/' + ou + '/Home', 'Co', 'Content');
					outHTML += getLink('https://mylo.utas.edu.au/d2l/lms/classlist/classlist.d2l?ou=' + ou, 'Cl', 'Classlist');
					outHTML += getLink('https://mylo.utas.edu.au/d2l/lp/manageFiles/main.d2l?ou=' + ou, 'F', 'File Manager');
					outHTML += getLink('https://mylo.utas.edu.au/d2l/le/' + ou + '/discussions/List', 'Di', 'Discussions');
					outHTML += getLink('https://mylo.utas.edu.au/d2l/lms/dropbox/dropbox.d2l?ou=' + ou, 'As', 'Assignments');
					outHTML += getLink('https://mylo.utas.edu.au/d2l/lms/group/group_list.d2l?ou=' + ou, 'Gp', 'Groups');
					outHTML += getLink('https://mylo.utas.edu.au/d2l/lms/grades/admin/manage/gradeslist.d2l?ou=' + ou, 'Gd', 'Grades');
					outHTML += getLink('https://mylo.utas.edu.au/d2l/lms/grades/admin/settings/personal_display_options.d2l?ou=' + ou, 'Gs', 'Grades Settings');
					outHTML += getLink('https://mylo.utas.edu.au/d2l/lms/quizzing/quizzing.d2l?ou=' + ou, 'Q', 'Quizzes');
					outHTML += getLink('https://mylo.utas.edu.au/d2l/lp/rubrics/list.d2l?ou=' + ou, 'Ru', 'Rubrics');
					outHTML += getLink('https://mylo.utas.edu.au/d2l/le/classlist/userprogress/12345/' + ou + '/settings/Edit', 'CS', 'Classlist Settings');
					outHTML += getLink('https://mylo.utas.edu.au/d2l/lms/importExport/import_export.d2l?ou=' + ou + '&amp%3bsp=1', 'IE', 'Import/Export Components') + '</div>';
					o.after(outHTML); 
				}
			}		
		});
		// If there are rows in the grid, then add our Quicklinks legend to the bottom.
		if ($('td[aria-describedby="grdMyUnits_UnitTitle"] a').length>0) $('#SearchFieldsDiv').after('<div class="quicklink"> Co = Content  &nbsp;   Cl = Classlist &nbsp;    F = File Manager  &nbsp;   Di = Discussions  &nbsp;   As = Assignments  &nbsp;   Gp = Groups &nbsp;   Gd = Grades    &nbsp;   Gs = Grades Settings   &nbsp;   Q = Quizzes &nbsp;  Ru = Rubrics &nbsp;  CS = Classlist Settings &nbsp;  IE = Import/Export Components</div>');
	}

	if (store.manager.adjustTable) { 
		$('#content-wrapper,#wrapper').css('width','100%');	// Set the parent display containers to 100% width.
		resizeGrid(); // The grid doesn't currently allow percentages, so it needs to have the pixel size set whenever the window is resized. It also means it needs to be set here, for the first time.
	}

	// If the 'number of rows to display' value is different to the one we've got stored, then we'll want to change that here.
	if ($('#grdMyUnits').getGridParam('rowNum')!=store.manager.recordsPerPage) {
		store.manager.recordsPerPage=$('#grdMyUnits').getGridParam('rowNum');
	}
	
}

/**
 * A shorthand tool to generate an `<a href=""></a>` tag.
 * 
 * @param {string} link The URL for the hyperlink.
 * @param {string} txt The text to display for the link.
 * @param {string} titl The tooltip text.
 * @returns {string} HTML string.
 * @memberof MyLO Manager#
 */
function getLink(link, txt, titl) {
	return ' <a href="' + link + '" target="_blank" title="' + titl + '">' + txt + '</a> ';
}

/**
 * Resizes the grid to be the full width of the screen
 * @memberof MyLO Manager#
 */
function resizeGrid() {
	$('#grdMyUnits').jqGrid('setGridWidth', $('#SearchFieldsDiv').width()-20); // Take 20px off the width to prevent overflow (unknown why this is needed, but it is)
}

// When the window is resized, resize the grid with it.
// The grid doesn't accept percentage-based sizes, only pixels, so it has to be done like this. 
$(window).on('resize', resizeGrid);

// Run when the document is ready.
$(function() {
	// Whenever the grid is updated (search critera changed, etc), refresh the quicklinks	
	$('#grdMyUnits').on('jqGridGridComplete', addQuicklinks); 

	// Bulk User Management (BUM) export set up
	if (store.manager.addBUM) {
		$('#grdMyUnits').jqGrid('navButtonAdd', '#pagerMyUnits', { caption:'Bulk User Management (BUM)',title:'Export page for Bulk User Management (BUM) tool',buttonicon :'ui-icon-arrowthickstop-1-n', onClickButton: function() {dialog.dialog('open');}} ); // Add button for Bulk User Management (BUM) export
		$('body').append('<div id="BUMdialog" title="Export Bulk User Management (BUM) CSV"><form><fieldset><p>Please enter the usernames you\'d like to include, and the role you\'d like them to have (comma separated).</p><p><label for="names">Usernames:</label><input type="text" name="names" id="BUMnames" value="" class="text ui-widget-content ui-corner-all"> <select id="BUMrole" role="listbox"><option role="option" value="Support" selected>Support</option><option role="option" value="Lecturer">Lecturer</option></select><input type="submit" tabindex="-1" style="position:absolute; top:-1000px"></p><p><strong>Note</strong> only the rows on the current page will be included.</p><p><a id="exportcsv" style="display:none" href="javascript:void()">Export CSV</a></p></fieldset></form></div>');
		$('#BUMnames').on('input',doBUM);
		dialog = $( '#BUMdialog' ).dialog({
			autoOpen: false,
			height: 250,
			width: 400,
			modal: true,
			buttons: {
				Cancel: function() {
					dialog.dialog('close');
				}
			} 
		});
	}
	// end Bulk User Management (BUM) export setup

	if(store.manager.displayRowColours) {
		// If 'Display Row Colours' is true, then we'll add background-colors to the rows, based upon the start and end date of the unit.
		$('#grdMyUnits').on('jqGridGridComplete', function() { 
			// Gets all the rows from the grid, except for the first row (the headers).
			const rows = document.getElementById('grdMyUnits').querySelectorAll('tr[role="row"]:not(.jqgfirstrow)');
			for(var i = 0; i < rows.length; i++) {
				// 	Go through each row, and convert its AccessStart and AccessEnd dates to Javascript `Date` objects. 
				//	With Date objects, we can use comparitor symbols (>, <, etc).
				//	
				// 		If the unit in this row has an end date that is in the past, then give it the class 'row-old-date'.
				//	
				// 		If the unit in this row has a start date that is in the past, and an end date that is in the future,
				//			then give it the class 'row-current-date'.
				//	
				// 		If the unit in this row has a start date that is in the future, then give it the class 'row-new-date'.
				//	
				// 	These row color definitions are configured in `src/css/content.css`.
				const row = rows[i];
				const now = Date.now();
				const start = new Date(row.querySelector('[aria-describedby="grdMyUnits_AccessStart"]').innerText);
				const end = new Date(row.querySelector('[aria-describedby="grdMyUnits_AccessEnd"]').innerText);
				if(end < now) { row.classList.add('row-old-date'); }
				else if(start > now) { row.classList.add('row-new-date'); }
				else if(start < now && end > now) { row.classList.add('row-current-date'); }
				if(store.other.makeAccessible) row.classList.add('row-accessible');
			}
		});
	}

	if ($('#grdMyUnits').getGridParam('rowNum')!=store.manager.recordsPerPage) { 
		$('#grdMyUnits').jqGrid('setGridParam',{'rowNum': store.manager.recordsPerPage}).trigger('reloadGrid'); // also triggers addQuicklinks
		$('#pg_pagerMyUnits .ui-pg-selbox').val(store.manager.recordsPerPage); 
	} else addQuicklinks(); // When the page is loaded, the grid has already rendered, so there are no events to catch. However, we know that the grid has loaded, so just insert the quick links

	// When the 'Rows to display' value changes, let's send that value to our background script to remember.
	$('#pg_pagerMyUnits .ui-pg-selbox').on('change', function() {
		chrome.runtime.sendMessage(extensionID, {request: 'updateRecord', records: $(this).val()});
	});
});
/**
 * Launched by the #BUMtool button in MyLO Manager. Exports a CSV of units on the page, along with user IDs.
 * @memberof MyLO Manager#
 */
function doBUM() { 
	// The CSV output
	var outCSV = '';
	// Takes in the comma-seperated usernames, and splits and trims then into an array.
	var users = $('#BUMnames').val().split(',').map((item) => item.trim()); 
	// If there are users and the Bulk User Management (BUM) names field isn't empty (after getting rid of any whitespace)
	if ((users.length>0) && ($('#BUMnames').val().trim() !=='')) { 
		var today = new Date();
		// Go through each unit in the grid currently, and get each AccessEnd date.
		// If it's valid, then get the ID of the unit, and give the user a role (Past Support | Support, depending when the unit ends),
		// and add the details to a CSV string.
		$('#grdMyUnits > tbody > tr').each(function() {
			var o = $(this);var accessEndString = $('td[aria-describedby="grdMyUnits_AccessEnd"]',o).prop('title');
			if (accessEndString) { // Ensure only rows with a valid end date are included 
				var accessEnd = $.datepicker.parseDate('dd/M/yy', accessEndString);
				var thisID = $('td[aria-describedby="grdMyUnits_OrgUnitID"]',o).prop('title');
				$.each(users,function(i,v) {
					outCSV += 'ENROLL,' + v + ',,' + (accessEnd < today?'Past Support':'Support') + ',' + thisID + '\r\n';
				});
			}
		});
		// If we have data in our CSV string, add the headers,
		// then generate a Blob and a URL to download it.
		if (outCSV!=='') {
			outCSV = 'ENROLL,Username,,Role Name,Org Unit Code\r\n'+outCSV; 
			var blob = new Blob([outCSV], { type: 'text/csv;charset=utf-8;' }); 
			var url = URL.createObjectURL(blob);
			$('#exportcsv').attr({'href':url,'download':'QuicklinksBUM.csv'}).fadeIn();  
		}  
	} else $('#exportcsv').hide(); // There were no valid users, so hide the exportCSV panel.
}
