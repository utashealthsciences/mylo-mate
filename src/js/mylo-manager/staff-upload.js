// The console-hack for a bulk staff add!
//
// 	This tool is currently designed for use in the debug console.
//	It will eventually be refined and integrated into the MyLO plugin, though for now it is usable (to a degree, at least!).
//	
//	USAGE:
//		Step 1: Paste the two functions into the console editor and hit enter.
//		Step 2: Add the users array variable into the console as well, and hit enter. (if pre-prepared, these two steps can be done at once.)
//		Step 3: Open the "Search Users" panel by clicking on the "Search" button in the Staff tab of Mylo Manager.
//		Step 4: Once the search panel is open, paste the `forEach` function block into the console and hit enter. 
//				This will iterate over each user in the array, retreive their information and insert it into the panel for you to select/unselect (by default, all entries are selected).
//		Step 5: (Optional) Once users have been added, run the `$("#grdStaff select").each` loop to go through and change all "Auditor" roles to a role of your choosing.
//				Auditor is not a common role, but make sure you check if there are any people meant to be an Auditor before you run this function!
//
//
//	Designed by Connor Deckers (cdeckers)

// The request to send off in order to get the desired details for each person.
function GetDataFromStaffUsername(username) {
	return new Promise(function(resolve, reject) {
		$.ajax({
			url: 'https://mylo-manager.utas.edu.au/User/StaffSearch?Surname=&GivenName=&Username=' + username + '&DepartmentCode=&StaffId=',
			dataType: 'json',
			type: 'POST',
			data: {
				_search: false,
				nd: 1501635126034,
				rows: 100,
				page: 1,
				sidx: 'givenname',
				sord: 'asc'
			},
			success: function (data) {
				// NOTE:
				// 		ALL STAFF DETAILS SHOULD BE RETREIVED BY STAFF USERNAME ONLY.
				//		THIS IS TO REMOVE THE RISK OF DUPLICATE RECORDS!
				data = data.rows.filter(function(row) { return row.id == username; })[0];
				resolve({
					id: data.id,
					PreferredName: data.cell[0],
					Surname: data.cell[1],
					Username: data.cell[2],
					StaffID: data.cell[3],
					Schools: data.cell[4]
				});
			}, 
			error: reject
		});
	});
}

// Adds results to the search-box staff list
function AddStaffToList(data) {
	$('#grdDialogResults').addRowData(data.id, {
		'PreferredName': data.PreferredName,
		'Surname': data.Surname,
		'Username': data.Username,
		'StaffID': data.StaffID,
		'Schools': data.Schools,
		'Select': `<input type="checkbox" checked="checked" class="ui-corner-all" id="chkSelect${data.id}"/>`
	});
}

// The list of users to add to the page
const users = ['agarrott', 'alowando', 'alwarren', 'annak2', 'ajusup', 'bheywood', 'carolyn', 'celliss', 'cherylh', 'csmyth', 'ctubman', 'cmoy', 'clarkcm', 'wardropd', 'emmaa0', 'fionam18', 'gef', 'jcave', 'janes3', 'jrstephe', 'jlinglis', 'jakemp', 'hainesj', 'kjylee', 'klfoster', 'kpashton', 'kim_mb', 'kristinp', 'laura_jh', 'lmsun', 'lgranger', 'mfo', 'msmith3', 'no', 'nsevans', 'paulb7', 'peter_b', 'rachel', 'rmaynard', 'raleaver', 'rjemery', 'sdp', 'slbward', 'mhingsto', 'grahaml', 'andream6', 'jkmcgee', 'staylor8' ];

// For each user, get their data from the server and then add them to the staff list.
users.forEach(function(user) {
	GetDataFromStaffUsername(user).then(function(data) {
		AddStaffToList(data);
	});
});

// Go through the select list, and choose the role you want all Auditor roles to be set to 
// NOTE: 
//		Auditor is the default, and rarely used. This is no excuse for bad code though, 
//		and will be redesigned to work more appropriately (either finding the rows added based on their ID's, or setting them directly on the server.)

$("#grdStaff select").each(function (index, sel) {
	if ($(sel).val() == "Auditor") $(sel).val('Student');
});