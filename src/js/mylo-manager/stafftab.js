$(function() {
	
	// Prevents the script from being run more than once.
	if (window.guardactive) return;
	window.guardactive = true;

	
	var options = document.querySelectorAll('#cboDialogDepartment option'); var maxLen = 80;
	options.forEach(function (o) {
		o.title = o.innerText;
		if (o.innerText.length > maxLen) {
			var str = o.innerText.substr(0, (maxLen / 2) - 3);
			str += '...';
			str += o.innerText.substr((maxLen / 2) * -1);
			o.innerText = str;
		}
	});
	
	// Hijacking a little space here for user-list comms.
	document.addEventListener('USERCONTENT_RPC', function(e) {
		const d = e.detail;
		if(d.flag === 'GET') {

			let p = [];
			const users = $('#grdDialogResults').jqGrid().getDataIDs();
			for(let user of users) {
				const r = $('#grdDialogResults').jqGrid().getRowData(user);
				r.checked = document.querySelector(`#chkSelect${user}`).checked;
				p.push(r);
			}

			document.dispatchEvent(new CustomEvent('USERGROUPS_RPC', { detail: {
				payload: p,
				ts: d.ts
			}}));
		}
		else if(d.flag === 'SET') {
			$('#grdDialogResults').jqGrid().clearGridData();
			d.payload.forEach((data) => {
				$('#grdDialogResults').addRowData(data.Username, {
					'PreferredName': data.PreferredName,
					'Surname': data.Surname,
					'Username': data.Username,
					'StaffID': data.StaffID,
					'Schools': data.Schools,
					'Select': `<input type="checkbox" checked="checked" class="ui-corner-all" id="chkSelect${data.Username}"/>`
				});
			});
		}
	});

	$('#grdDialogResults').on('jqGridGridComplete', function() {
		document.dispatchEvent(new CustomEvent('GridReady'));
	});
});

