/**
 * @namespace Echo360
 */
$(function() {
	// The window guard prevents this script from being run more than once without being reloaded. 
	// This is to prevent any duplications issues.
	if(window.guardactive) return;
	window.guardactive = true;
	
	/**
	 * Finds the React properties attached to a DOM element.
	 * 
	 * @param {any} dom The DOM element to get React properties from.
	 * @returns {(object|null)} The React properties of the object, or null if none found.
	 * @memberof Echo360#
	 */
	function FindReact(dom) {
		for (var key in dom) {
			if (key.startsWith('__reactInternalInstance$')) {
				var compInternals = dom[key]._currentElement;
				var compWrapper = compInternals._owner;
				var comp = compWrapper._instance;
				return comp;
			}
		}
		return null;
	}

	/**
	 * Checks the properties of the rows, and retrieves the downloadable links and names from each element.
	 * 
	 * @returns {object} Object containing the unit name, offering period (e.g. 2017_S2-ALL), and the videos found.
	 * @memberof Echo360#
	 */
	function checkProperties(port) {

		return new Promise((resolve, reject) => { 
			// An array to hold our video elements in.
			let videos = [];
			const rows = document.querySelectorAll('.contents-wrapper [role="link"]');
	
			const ping = (mediaID, sectionID) => new Promise((resolve, reject) => {
				let x = new XMLHttpRequest();
				x.addEventListener('load', resolve);
				x.addEventListener('error', reject);
				x.open('GET', 'https://echo360.org.au/media/' + mediaID + '/download-modal?sectionId=' + sectionID);
				x.send();
			});
	
			let userID = window.Echo.userId;
			let stack = [];
			rows.forEach(row => {
				let p = FindReact(row).props;
				let media = p.lesson.medias[0];
	
				if (media.mediaType != 'Video') return;
				if (!media.isDownloadable) return;
	
				let url = 'https://content.echo360.org.au';
				url += '/' + (new URL(p.lesson.coverImage).pathname).substring(1, 5) + '.' + p.info.institutionId;
				url += '/' + media.id;
				url += '/hd1.mp4';
				url += '?x-uid=' + userID;
				url += '&x-mid=' + media.id;
				url += '&x-instid=' + p.info.institutionId;
				url += '&x-act=mediaDownload&x-src=desktop';
				
				ping(media.id, p.sectionId).then(() => {
					port.postMessage({
						flag: 'download', 
						data: {
							unit: document.getElementsByClassName('course-section-header')[0].getElementsByTagName('h1')[0].innerText.split('-')[0].trim(),
							offering: FindReact(rows[0]).props.lesson.userSection.sectionNumber,
							videos: [{ title: p.lesson.title, url }]
						}
					});
				});
			});
		});

		// For each row in the list
		// for(let i = 0; i < rows.length; i++) {
			
		// 	// Find the React object and get the properties from it.			
		// 	const r = FindReact(rows[i]); const p = r.props;

		// 	// Get the title from the properties.
		// 	const title = p.title;
		// 	if(p.lesson.video) {
		// 		// There are no video files here, but there are audio files.
		// 		// We have an audio file to download instead!
		// 		if(	p.lesson.video.media.media.current.primaryFiles.length == 0 && p.lesson.video.media.media.current.audioFiles ) { 

		// 			// Get the audio file with the biggest size (generally, the highest quality version), then push the data to the end of our array.
		// 			const audio = p.lesson.video.media.media.current.audioFiles.reduce((prev, current) => (prev.size > current.size) ? prev : current, {size: 0});
		// 			videos.push({title: title, url: audio.s3Url});

		// 		}
		// 		else if(p.lesson.video.media.media.current.primaryFiles && p.lesson.video.media.media.current.primaryFiles.length > 0) {
					
		// 			// Get the highest resolution video, then push the data to the end of our array.
		// 			const video = p.lesson.video.media.media.current.primaryFiles.reduce((prev, current) => (prev.width > current.width) ? prev : current, {width:0});
		// 			videos.push({title: title, url: video.s3Url});

		// 		}
		// 	}
		// }
		
		// return {
		// 	unit: document.getElementsByClassName('course-section-header')[0].getElementsByTagName('h1')[0].innerText.split('-')[0].trim(),
		// 	offering: FindReact(rows[0]).props.lesson.userSection.sectionNumber,
		// 	videos: videos
		// };
	}

	// Open a communications channel with the background script.
	const p = chrome.runtime.connect(extensionID, {name: 'echo360-download-listener'});

	// Post a greeting to open the network on the other side, so it can start sending things back!
	p.postMessage({flag: 'init'});
	
	// When we get the request to start downloading..
	p.onMessage.addListener(function(msg) {
		// If the request is specifically for a download, 
		// then send back a message with the same flag, 
		// and with our collection of videos.
		if(msg.request == 'download') {
			checkProperties(p);
		}
	});
});