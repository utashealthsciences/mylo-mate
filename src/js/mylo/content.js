(function() {
	if(window.MyLORun) return;
	window.MyLORun = true;

	let top = document.createElement('button');
	top.id='top';
	top.className ='btn';
	top.title='Back to Top';
	top.setAttribute('aria-hidden', 'true');
	top.setAttribute('is', 'd2l-icon-button');
	top.innerHTML = `<d2l-icon class="style-scope d2l-icon-button d2l-icon-0" icon="d2l-tier1:chevron-up"></d2l-icon><span class="style-scope d2l-icon-button"></span>`;
	document.body.appendChild(top);

	top.addEventListener('click', function(e) { window.scrollTo(0,0); });

	let invisible = 200;
	let fadeIn = 300;

	let Y = null;
	function update() {
		if(Y < invisible) { top.style.opacity = 0; return; }
		if(Y > fadeIn) { top.style.opacity = 1; return; }
		if(Y > invisible && Y < fadeIn) {
			let scale = ((Y - invisible) * 100) / (fadeIn - invisible);
			top.style.opacity = scale / 100;
		}
	}

	setInterval(function() { let oldY = Y; Y = window.scrollY; if(oldY !== window.scrollY) update(); }, 10);
})();