/** @namespace Chrome/Background */

/**
 * Returns a regexp string based on * wildcards in the argument.
 * 
 * @author Connor Deckers (cdeckers)
 * @param {String} rule 
 * @returns new RegExp
 * @memberof Chrome/Background#
 */
function wildcard(rule) {
	return new RegExp('^' + rule.split('*').join('.*') + '$');
}

// The URL for a new tab. 
// This may change in future, and at the moment, Chrome doesn't 
// have a way of using the API to detect a new tab any other way.
const __NEWTAB = 'chrome://newtab/';

// A list of urls that are considered valid for the plugin to attach to.
// 		These are just the base URLS, paths aren't used here.
// This are used for the links panel, with the ID referencing the id passed from the popup panel.
const urls = {
	'mylo': 'https://mylo.utas.edu.au/',
	'mylo-manager': 'https://mylo-manager.utas.edu.au/',
	'echo360': 'https://echo360.org.au',
	'DEV2': 'https://d2ldev2.utas.edu.au' 
};

/**
 * An object containing the list of scripts to load, depending on the current tabs URL.
 * Note, you can use more than one entry per domain.
 * @example <caption>Single entry</caption>
 * const tabs = [{
 * 		// The ID of the element.
 * 		id: 'mylo-editor',
 * 		// The urls to match against. * can be used as a wildcard element.
 * 		urls: ['https://mylo.utas.edu.au/d2l/le/content/*\/authorHtml*', 'https://mylo.utas.edu.au/d2l/le/content/*\/EditFile*', 'https://d2ldev2.utas.edu.au/d2l/le/content/*\/authorHtml*', 'https://d2ldev2.utas.edu.au/d2l/le/content/*\/EditFile*'],
 *		// DON'T RUN if the URL matches one of these paths.
 *		blacklist: ['https://mylo.utas.edu.au/d2l/home','https://d2ldev2.utas.edu.au/d2l/home'],
 *		// The scripts to inject into the page (are run in the context of the DOM)
 * 		injected: ['https://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css', 'css/editor.css', 'js/mylo-editor/pagescript.js'],
 * 		// The scripts to run as content scripts (are run in the context of the plugin).
 *		content: []
 *	}];
 * @example <caption>Multi-path scripts</caption>
 * const tabs = [{
 * 		// The ID of the element.
 * 		id: 'mylo-editor',
 * 		// The urls to match against. * can be used as a wildcard element.
 * 		urls: ['https://mylo.utas.edu.au/d2l/le/content/*\/authorHtml*', 'https://mylo.utas.edu.au/d2l/le/content/*\/EditFile*', 'https://d2ldev2.utas.edu.au/d2l/le/content/*\/authorHtml*', 'https://d2ldev2.utas.edu.au/d2l/le/content/*\/EditFile*'],
 *		// DON'T RUN if the URL matches one of these paths.
 *		blacklist: ['https://mylo.utas.edu.au/d2l/home','https://d2ldev2.utas.edu.au/d2l/home'],
 *		// The scripts to inject into the page (are run in the context of the DOM)
 * 		injected: ['https://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css', 'css/editor.css', 'js/mylo-editor/pagescript.js'],
 * 		// The scripts to run as content scripts (are run in the context of the plugin).
 *		content: []
 *	},
 * 	{
 * 		// The ID of the element.
 *		id: 'mylo',
 * 		// The urls to match against. * can be used as a wildcard element.
 *		urls: ['https://mylo.utas.edu.au/d2l/*','https://d2ldev2.utas.edu.au/d2l/*'],
 *		// DON'T RUN if the URL matches one of these paths.
 *		blacklist: ['https://mylo.utas.edu.au/d2l/home','https://d2ldev2.utas.edu.au/d2l/home'],
 *		// The scripts to inject into the page (are run in the context of the DOM)
 *		injected: [],
 * 		// The scripts to run as content scripts (are run in the context of the plugin).
 *		content: ['js/mylo/content.js']
 *	}];
 * @memberof Chrome/Background#
 * @type {Types#TabData[]}
 * @see {@link Types#TabData}
 */
const tabs = [
	{
		id: 'mylo',
		urls: ['https://mylo.utas.edu.au/d2l/*', 'https://d2ldev2.utas.edu.au/d2l/*'],
		injected: ['css/mylo.css'],
		content: ['js/mylo/content.js']
	},
	{
		id: 'mylo-editor',
		urls: ['https://mylo.utas.edu.au/d2l/le/content/*/authorHtml*', 'https://mylo.utas.edu.au/d2l/le/content/*/EditFile*', 'https://d2ldev2.utas.edu.au/d2l/le/content/*/authorHtml*', 'https://d2ldev2.utas.edu.au/d2l/le/content/*/EditFile*'],
		injected: ['https://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css', 'css/editor.css', 'js/mylo-editor/pagescript.js'],
		content: []
	},
	{
		id: 'mylo-grades',
		urls: ['https://mylo.utas.edu.au/d2l/lms/grades/admin/enter/user_list_view.d2l*', 'https://d2ldev2.utas.edu.au/d2l/lms/grades/admin/enter/user_list_view.d2l*'],
		injected: [],
		content: ['js/mylo-grades/enter-grades.js']
	},
	{
		id: 'mylo-grade-mark',
		urls: ['https://mylo.utas.edu.au/d2l/lms/group/group_enroll.d2l?*', 'https://d2ldev2.utas.edu.au/d2l/lms/group/group_enroll.d2l?*'],
		injected: [],
		content: ['js/mylo-grades/script.js']
	},
	{
		id: 'mylo-manage-files',
		urls: ['https://mylo.utas.edu.au/d2l/lp/manageFiles/main.d2l*', 'https://d2ldev2.utas.edu.au/d2l/lp/manageFiles/main.d2l*'],
		injected: ['css/manageFiles.css'],
		content: ['js/mylo-files/script.js']
	},
	{
		id: 'mylo-units',
		urls: ['https://mylo.utas.edu.au/d2l/*','https://d2ldev2.utas.edu.au/d2l/*'],
		blacklist: ['https://mylo.utas.edu.au/d2l/home','https://d2ldev2.utas.edu.au/d2l/home'],
		injected: ['css/mylo-units.css'],
		content: ['js/mylo-units/content.js']
	},
	{
		id: 'mylo-rubric',
		urls: ['https://mylo.utas.edu.au/d2l/lp/rubrics/criterion_newEdit.d2l?*', 'https://mylo.utas.edu.au/d2l/lp/rubrics/level_newEdit.d2l?*', 'https://mylo.utas.edu.au/d2l/lp/rubrics/edit_overallLevels.d2l?*'], // Needs to have Daylight added!
		injected: ['css/rubric.css', 'js/mylo-rubrics/rubric.js'],
		content: []
	},
	{
		id: 'mylo-search',
		urls: ['https://mylo.utas.edu.au/d2l/*','https://d2ldev2.utas.edu.au/d2l/*'],
		injected: ['css/mylo-search.css'],
		content: ['js/mylo-search/content.js']
	},
	{
		id: 'mylo-search-screen',
		urls: ['https://mylo.utas.edu.au/d2l/le/manageCourses/search/*','https://d2ldev2.utas.edu.au/d2l/le/manageCourses/search/*'],
		injected: ['css/mylo-search.css'],
		content: ['js/mylo-search/search-units.js']
	},
	{
		id: 'mylo-bulk-delete-discussions',
		urls: ['https://mylo.utas.edu.au/d2l/le/*/discussions/bulkdelete/List','https://d2ldev2.utas.edu.au/d2l/le/*/discussions/bulkdelete/List'],
		injected: [],
		content: ['js/mylo-discussions/fixsearch.js']
	},
	{
		id: 'mylo-bulk-discussions',
		urls: ['https://mylo.utas.edu.au/d2l/le/*/discussions/List','https://d2ldev2.utas.edu.au/d2l/le/*/discussions/List'],
		injected: [],
		content: ['js/mylo-discussions/add-shortcuts.js']
	},
	{
		id: 'mylo-manager',
		urls: ['https://mylo-manager.utas.edu.au/', 'https://mylo-manager.utas.edu.au/Home'],
		injected: ['css/content.css', 'js/mylo-manager/pagescript.js'],
		content: []
	},
	{
		id: 'mylo-manager-staff',
		urls: ['https://mylo-manager.utas.edu.au/UnitRequest/Staff/*'],
		injected: ['css/jquery-ui.min.css', 'css/jquery-ui.jqui.min.css', 'css/manager.css', 'js/mylo-manager/stafftab.js'],
		content: ['app/dataset.js', 'js/GroupManagement.js', 'lib/jquery-ui.min.js', 'js/mylo-manager/users.js']
	},
	{
		id: 'echo360',
		urls: ['https://echo360.org.au/*'],
		injected: ['js/echo360/pagescript.js'],
		content: []
	}
];

/**
 * Gets an insertable URL, based on the file provided.
 * If this URL is an external file (starts with `http`), then it will return,
 * 	otherwise it will get the `chrome-extension://` URL.
 * @param {string} url The URL of the file to insert
 * @returns {string} The insertable URL link.
 * @memberof Chrome/Background#
 */
const getURL = (url) => {
	if(url.substr(0,4) == 'http') { return url; }
	else { return chrome.runtime.getURL(url); }
};

// When a tab is activated, check the URL of the tab and find out if we should make our icon appear 'enabled'.
chrome.tabs.onActivated.addListener(function(info){
	chrome.tabs.get(info.tabId, function(change){ 
		changeIconBasedOnTab(change.url,info.tabId); 
	});
});

// When a tab is updated, check the URL of the tab and find out if we should make our icon appear 'enabled'.
// Check to also see if we have some functionality to run on this tab, and if we do, inject or apply our scripts.
chrome.tabs.onUpdated.addListener(function (tabId, change, tab){
	changeIconBasedOnTab(tab.url,tabId); 
	var tabData = checkURL(tab.url);
	if(tabData.length > 0) { tabData.forEach((t) => {
		t.injected.map(function(url) { sendMessage(tabId, 'injectScript', getURL(url)); });
		t.content.map(function(url) { chrome.tabs.executeScript(tabId, {file: url}); });
	}); }
});

/**
 * A wrapper function to make changing our icon a little easier.
 * 
 * @param {string} url The URL of the current tab.
 * @param {number} tabId The tab ID received from the Chrome event. This allows the extension to know what tab to change the appearance for.
 * @see {@link changeIcon} for more information.
 * @memberof Chrome/Background#
 */
function changeIconBasedOnTab(url,tabId) {
	changeIcon(checkURL(url).length > 0, tabId);
}

/**
 * Enables or disables the icon.
 * 
 * @param {boolean} enableIcon The state to set the icon to.
 * @param {number} id The tabID to change the icon for.
 * @memberof Chrome/Background#
 */
function changeIcon(enableIcon,id) {
	if (enableIcon) chrome.browserAction.setIcon({path: '../images/icon48.png', tabId: id});
	else chrome.browserAction.setIcon({path: '../images/icon128d.png', tabId: id});
}

/**
 * Gets the appropriate dataset from our tabs object
 * 
 * @param {any} url 
 * @returns {boolean|Types#TabData} Returns false if the URL is not defined, otherwise will return an array containing all the {@link Types#TabData} results that match the current URL.
 * @see {@link Chrome/Background#tabs}
 * @memberof Chrome/Background#
 */
function checkURL(url) {
	if(url === undefined) { return false; }
	let matching = [];
	for(var i = 0; i < tabs.length; i++) {		// For each tab we want to compare against
		const whitelisted = matches(url, tabs[i].urls);
		const blacklisted = (tabs[i].blacklist ? matches(url, tabs[i].blacklist) : false);
		if(whitelisted && !blacklisted) { matching.push(tabs[i]); }
	}
	return matching;
}

/**
 * Tests if a string matches an array of strings.
 * 
 * @param {string} url The URL to test.
 * @param {string[]} urls The array of URLs to test against.
 * @returns {boolean} Returns true if any URLs match.
 * @memberof Chrome/Background#
 */
function matches(url, urls) {
	for(var i = 0; i < urls.length; i++) { if(url.match(wildcard(urls[i]))) return true; }
}

/**
 * Sends a message to a tab, with a handshake message (the name on the envelope, if you will), 
 * the data, and a response callback.
 * 
 * @param {number} tabId The tab to send the message to.
 * @param {string} handshake The handshake message.
 * @param {any} data The data to send with the message.
 * @param {function} response The callback to run when there's a response.
 * @memberof Chrome/Background#
 */
function sendMessage(tabId, handshake, data, response) {
	if(response === undefined) { response = function(){}; }
	chrome.tabs.sendMessage(tabId, {handshake: handshake, data: data}, response);
}

// Listens for messages from external sources (message requests from DOM-based scripts [injected scripts!] are considered external)
// and handle them appropriately.
chrome.runtime.onMessageExternal.addListener(function(request, sender, sendResponse) {
	switch(request.request)
	{
		// If we get a request to update records from MyLO Manager,
		// handle it here.
		case  'updateRecord':
			chrome.storage.sync.get({manager: {}}, function(data) {
				data.recordsPerPage = request.records;
				chrome.storage.sync.set({ manager: data });
			});
			break;
	}
});

/**
 * Opens a URL, based on the ID passed to it.  
 * 	Note: This will overwrite a "new tab" if open, otherwise will auto open in a new tab.
 * @param {string} id The ID of the URL to open.
 * @memberof Chrome/Background#
 */
const openPage = (id) => {
	if(urls[id] !== undefined) {
		chrome.tabs.query({active:true,currentWindow:true}, function(tab) {
			if(tab[0].url == __NEWTAB) {
				chrome.tabs.update(  tab[0].id, {url: urls[id]} );
			}
			else {
				chrome.tabs.create({ url: urls[id] });
			}
		});
	}
};

/**
 * Downloads the videos listed in the dataset.  
 * Default action when encountering duplicate filenames is [uniquify]{@linkcode https://developer.chrome.com/extensions/downloads#type-FilenameConflictAction}.
 * @param {Types#VideoDownloads} msg The download dataset.
 * @memberof Chrome/Background#
 */
const downloadVideos = (msg) => {

	const safe = (text) => text.replace(/[\\/:*?"<>|]/g, '');

	const suggestedDirectory = 'Echo360 Downloads/' + safe(msg.data.unit) + '-' + safe(msg.data.offering) + '/';
	msg.data.videos.forEach((video) => {
		let file = video.url.split('?')[0].trim();
		let ext = file.split('.');
		ext = ext[ext.length-1].trim();
		chrome.downloads.download({
			url: video.url, 
			filename: suggestedDirectory + safe(video.title).replace(/[\s]+/g, ' ') + '.' + ext, 
			conflictAction: 'uniquify'
		});
	});
};

// A container to store active messaging ports in.
let ports = {
	'echo360': null,
	'mylo-editor': null,
	'mylo-manager': null
};

// Listens for requests from external sources; e.g. injected scripts!
chrome.runtime.onConnectExternal.addListener(function(p) {
	// When we get a handshake from our download port opening, store it so that we can send it the all-clear later on.
	if(p.name == 'echo360-download-listener') {
		ports['echo360'] = p;
		p.onMessage.addListener((msg) => {
			if(msg.flag == 'download') { downloadVideos(msg); }
		});
	}
	// If we get a handshake from our ICB tool port opening, store it so that we can toggle the ICB sidebar from our popup later.
	else if(p.name == 'icb-listener') {
		ports['mylo-editor'] = p;
	}
});

/**
 * Performs an action, based on an ID passed from our popup windows contextual pane.
 * @param {string} request The ID to parse against.
 * @memberof Chrome/Background#
 */
const handleContextAction = (request) => {
	switch(request) {
		case 'mylo-icb': {
			if(ports['mylo-editor'] !== null) {
				ports['mylo-editor'].postMessage({request: 'toggleVisibility'});
			}
			break;
		}
		case 'all-links-in-new-tab': {
			if(ports['mylo-editor'] !== null) {
				ports['mylo-editor'].postMessage({ request: 'all-links-in-new-tab'});
			}
			break;
		}
		case 'echo360-download': {
			if(ports['echo360'] !== null) {
				ports['echo360'].postMessage({request: 'download'});	
			} 
			break;
		}
	}
};

function ping(port, data) {
	return new Promise(function (reject, resolve) {
		port.postMessage({ flag: '_sync', request: data });
		
		function d(data) {
			if(data.flag == '_sync') {
				port.onMessage.removeListener(d);
				resolve(data);
			}
		}

		port.onMessage.addListener(d);
	});	
}

// This is what handles the popup page!
chrome.runtime.onConnect.addListener(function(p) {
	
	if(p.name == 'popup') {
		p.onMessage.addListener(function(msg) {
			if(msg.action == 'open') { openPage(msg.data); }
			if(msg.action == 'context') { handleContextAction(msg.data); }
		});
	}

});

chrome.runtime.onMessage.addListener((req, s, res) => {
	if(req && req.open == 'options') {
		chrome.runtime.openOptionsPage();
	}
});