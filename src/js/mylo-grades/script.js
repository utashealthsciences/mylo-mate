$(function() {
	// Window guard
	// Prevents the script from being run more than once.
	if (window.guardactive) return;
	window.guardactive = true;

	document.querySelector('.d_gd tr.d_gh th:last-child label:nth-child(2)').innerHTML = `<br><input type="checkbox" title="Select all" id="select-all">`;

	const checkboxes = document.querySelectorAll('.d_gd tbody input[type="checkbox"].d_chb');

	document.querySelector('#select-all').addEventListener('click', e => {
		checkboxes.forEach(el => el.checked = e.target.checked);
	});

});