(function() {
	if(window.EnterGradeFix) return;
	window.EnterGradeFix = true;

	document.querySelectorAll('.action.style-scope.d2l-scroll-wrapper.x-scope.d2l-icon-button-0').forEach(elem => elem.remove());
	let marginRight = '30px';
	document.querySelector('.d2l-table-wrapper #wrapper').style.overflowX = 'visible';
	document.querySelector('.d2l-table-wrapper #wrapper > table').style.paddingRight = marginRight;
})();