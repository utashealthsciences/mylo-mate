/*global UI,Group,Groups,User,_,GetDataFromStaffUsername*/
function CheckEnabledButtons() {
	_('#group-actions .edit').setAttribute('disabled', !Groups.valid);
	_('#group-actions .delete').setAttribute('disabled', !Groups.valid);
	_('#user-actions .add').setAttribute('disabled', !Groups.valid);
}

function PushGroups(groups) {
	return new Promise(resolve => {
		chrome.storage.sync.get({
			'usergroups': null
		}, (data) => {
			chrome.storage.sync.set({
				'usergroups': {
					'enabled': data.usergroups.enabled,
					'groups': groups
				}
			}, resolve);
		});
	});
}

function UpdateGroups(groups, selected) {
	// Assign a handle for the Groups select box
	const groupSelect = _('#usergroups_groups');

	// Remove all the old entries
	for (let i = groupSelect.options.length - 1; i >= 0; i--) {
		if (!groupSelect.options[i].attributes.default) {
			groupSelect.options.remove(i);
		}
	}

	// Add the groups from the dataset
	for (const id in groups) {
		if (id.startsWith('_')) continue;
		const e = groups[id];
		let opt = document.createElement('option');
		opt.innerHTML = e.Name;
		opt.value = e.ID;
		if (e.ID == selected) opt.selected = true;
		groupSelect.appendChild(opt);
	}

	CheckEnabledButtons();

}

function UpdateUsers(users) {
	// Assign a handle for the users select panel.
	const select = _('#usergroups_users');

	// Remove all the old users
	for (let i = select.options.length - 1; i >= 0; i--) {
		if (!select.options[i].attributes.default) {
			select.options.remove(i);
		}
	}

	// For each of the users in the array, add them to the list.
	//	The option value is the staff username (e.g. cdeckers)
	//	The option text is FirstName Surname (username) {e.g. Connor Deckers (cdeckers)}
	users.forEach((user) => {
		let opt = document.createElement('option');
		opt.value = user.Username;
		opt.innerHTML = `${user.PreferredName} ${user.Surname} (${user.Username}) [${user.Role || 'Support'}]`;
		select.appendChild(opt);
	});

	if (users.length === 0) {
		_('#user-actions .edit').setAttribute('disabled', true);
		_('#user-actions .delete').setAttribute('disabled', true);
	}

	CheckEnabledButtons();
}


(function () {

	// Window guard
	// Prevents the script from being run more than once.
	if (window.GroupManagementRun) return;
	window.GroupManagementRun = true;

	if (window._ === undefined) window._ = (...rest) => document.querySelector(rest);

	document.addEventListener('Ready', (e) => {
		const { parent } = e.detail;

		const default_events = {
			group: {
				add: () => {
					UI.prompt('Group name', 'Please insert a group name. This can modified later in the Options panel.', 'Group name..').then((name) => {
						name = name.trim();
						if (name.length > 0) {
							const id = name.toLowerCase().replace(/[^\w\s\d]/gi, '').replace(/[\s]/g, '-');
							const group = new Group(id, name);
							Groups.get().then((data) => {
								if (data[id] === undefined) {
									data[id] = group;
									PushGroups(data).then(() => {
										UpdateGroups(data, id);
										UpdateUsers(data[id].Users);
										document.dispatchEvent(new CustomEvent('Group.Add'));
										document.dispatchEvent(new CustomEvent('Group.Modify'));
									});
								}
							});
						}
					}).catch(() => { });
				},
				edit: () => {
					Groups.get().then((groups) => {
						const curr = groups[Groups.current];
						const oldName = curr.Name;
						UI.prompt('Edit', 'Please enter a new name for this group.', '', curr.Name).then((newName) => {
							if (newName.trim() !== curr.Name.trim() && newName.trim().length > 0) {
								// We have a different name, so we'll update it here.
								curr.Name = newName.trim();

								PushGroups(groups).then(() => {
									UpdateGroups(groups);
									UpdateUsers(groups[Groups.current].Users);
									UI.notify('Success', `Group "${oldName}" renamed to "${newName.trim()}".`, { timeout: 2, parent, classes: 'center' });
									document.dispatchEvent(new CustomEvent('Group.Edit'));
									document.dispatchEvent(new CustomEvent('Group.Modify'));
								});
							}
						});
					});
				},
				delete: () => {
					Groups.get().then((groups) => {
						const curr = groups[Groups.current];
						UI.confirm('Are you sure?', `Are you sure you want to delete group "${curr.Name}"? <br><strong>This action cannot be undone.</strong>`).then(() => {
							delete groups[curr.ID];
							PushGroups(groups).then(() => {
								UpdateGroups(groups);
								UpdateUsers(groups[Groups.current] ? groups[Groups.current].Users : []);
								UI.notify('Success', `Group "${curr.Name}" deleted.`, { timeout: 2, parent, classes: 'center' });
								document.dispatchEvent(new CustomEvent('Group.Delete'));
								document.dispatchEvent(new CustomEvent('Group.Modify'));
							});
						}).catch(() => { });
					});
				}
			},
			user: {
				add: () => {
					UI.promptUserAdd('Add Users', 'Insert a list of usernames to add, seperated by a comma.<br>Example: <pre>username1, username2, username3</pre>', 'Usernames to add', '', 'Role for these users: ')
						.then(({ users, role }) => {

							if (!Array.prototype.unique) {
								Array.prototype.unique = function () {
									let seen = {};
									return this.filter(item => seen.hasOwnProperty(item) ? false : (seen[item] = true));
								};
							}



							// Break the usernames list into single usernames
							const usernames = users.split(',').filter(u => u.trim().length > 0).unique().map((u) => u.trim());

							// Get our groups
							Groups.get().then((groups) => {
							// Pull out our current group.
								const current = groups[Groups.current];

								// Get our list of existing usernames
								let existingUsers = current.Users.map(u => u.Username);

								// Builds a list of promises, by finding usernames that aren't part of the list yet
								//	and then doing a data request to the server.
								let stack = usernames
									.filter((username) => !existingUsers.includes(username) && username.trim() !== '')
									.map((user) => GetDataFromStaffUsername(user));

								// Process all the promises and wait until they're complete.
								Promise.all(stack).then((userData) => {

									let success = [];
									let errors = [];

									const users = userData.filter(u => {
										if (u.success) success.push(u.Username);
										else errors.push(u.username);
										return u.success;
									}).map(u => new User({
										...u,
										Role: role
									}));

									let message = [];

									if (users.length > 0) {
										Array.prototype.push.apply(current.Users, users);

										PushGroups(groups).then(() => {
											UpdateUsers(current.Users);
											message.push(`Users added to "${current.Name}".`);

											let messages = [];
											if (success.length > 0) messages.push(`Successfully added: ${success.map(u => `'${u}'`).join(',')}`);
											if (errors.length > 0) messages.push(`Failed to add: ${errors.map(u => `'${u}'`).join(',')}`);

											let title = 'Notice';
											if (success.length == 0) title = 'Error';
											if (errors.length == 0) title = 'Success';

											document.dispatchEvent(new CustomEvent('User.Add'));
											document.dispatchEvent(new CustomEvent('User.Modify'));

											UI.notify(title, messages.join('<br>'), { timeout: 2, parent, classes: 'center' });
										});
									} else if (errors.length > 0) {
										UI.notify('Error', 'Failed to add users: ' + errors.join(','), { timeout: 2, parent, classes: 'center' });
									}

								}).catch((err) => {
								// Something went wrong when we were collecting the data from the server!

									if (err.responseText.toLowerCase().includes('sign in')) {
										UI.notify('Error', 'You need to be logged in to MyLO Manager to add users.<br><br>Please open a new tab, sign in to MyLO Manager and try again.', { timeout: 5, parent, classes: 'center' });
									} else {
										UI.notify('Error', 'There was an error retrieving the data from the server. Please see the console for more information.', { parent });
										console.error(err);
									}
									options.get.then(opt => {
										if (opt.editor.showDevTools) {
											console.error(err);
										}
									});
								});
							});
						}).catch(() => { /* User cancelled the prompt. */ });

				},
				edit: () => {
					Groups.get().then(groups => {
						const curr = groups[Groups.current];
						const selected = [..._('#usergroups_users').options].filter((opt) => opt.selected).map((o) => o.value);
						UI.promptRoles('Edit User Roles', 'Edit user roles here', curr.Users.filter(u => selected.includes(u.Username))).then(users => {
							PushGroups(groups).then(() => {
								UpdateUsers(curr.Users);
								console.log(UI);
								UI.notify('Success', 'User roles updated successfully.', {timeout: 2, parent, classes: 'center'});
								document.dispatchEvent(new CustomEvent('User.Edit'));
								document.dispatchEvent(new CustomEvent('User.Modify'));
							});
						});
					});
				},
				delete: () => {
					// This action gets the options from the users box, and converts it into an array.
					//	Once an array, it will filter out only the selected elements, 
					//	and then map those down to just the values.
					//
					// This results in an array containing the usernames of the selected staff.
					const selectedUsers = [..._('#usergroups_users').options].filter((opt) => opt.selected).map((o) => o.value);

					Groups.get().then(groups => {
						const currGroup = groups[Groups.current];
						const currUsers = currGroup.Users;

						const users = currUsers.filter((user) => selectedUsers.includes(user.Username)).map((user) => user.PreferredName + ' ' + user.Surname);
						const lastUser = users.pop();

						const names = users.join(', ') + (users.length > 0 ? ' and ' : '') + lastUser;

						UI.confirm('Are you sure?', `Are you sure you want to delete ${names} from "${currGroup.Name}"? <br><strong>This action cannot be undone.</strong>`).then(() => {
							const remaining = currUsers.filter((user) => !selectedUsers.includes(user.Username));
							currGroup.Users = remaining;
							PushGroups(groups).then(() => {
								UpdateUsers(currGroup.Users);
								document.dispatchEvent(new CustomEvent('User.Delete'));
								document.dispatchEvent(new CustomEvent('User.Modify'));
								UI.notify('Success', `${names} removed from "${currGroup.Name}".`, { timeout: 2, parent, classes: 'center' });
							});
						}).catch(() => { });
					});

				}
			}
		};

		const events = $.extend(true, {}, default_events, e.detail.events);

		const options = {
			get get() {
				return new Promise(resolve => chrome.storage.sync.get(null, data => resolve(data)));
			}
		};

		/*********************************************
		 * 
		 * 
		 * 			USER / GROUP MANAGEMENT
		 * 
		 * 
		 *********************************************/

		// When the selected user group is changed, update the users list beneath it.
		_('#usergroups_groups').addEventListener('change', (e) => {
			Groups.get().then((data) => UpdateUsers(data[e.target.value].Users));
			CheckEnabledButtons();
		});

		// When the usergroup-group add button is clicked, add a new group.
		_('#group-actions .add').addEventListener('click', events.group.add);

		// When the usergroup-group edit button is clicked, edit the selected group.
		_('#group-actions .edit').addEventListener('click', events.group.edit);

		// When the usergroup-group delete button is clicked, remove the selected group.
		_('#group-actions .delete').addEventListener('click', events.group.delete);

		// When the usergroup-user add button is clicked, add a list of users.
		_('#user-actions .add').addEventListener('click', events.user.add);

		// When the usergroup-user edit button is clicked, edit the selected users.
		_('#user-actions .edit').addEventListener('click', events.user.edit);

		// When the usergroup-user delete button is clicked, remove the selected users.
		_('#user-actions .delete').addEventListener('click', events.user.delete);

		_('#usergroups_users').addEventListener('change', (e) => {
			const disabled = e.target.selectedOptions.length === 0;
			_('#user-actions .edit').setAttribute('disabled', disabled);
			_('#user-actions .delete').setAttribute('disabled', disabled);
		});
		/*********************************************
		 * 
		 * 
		 *		  END USER / GROUP MANAGEMENT
		 * 
		 * 
		 *********************************************/

		let items = e.detail.data;
		// Update the groups list
		UpdateGroups(items.usergroups.groups);

		// Update the users list with the users from the currently selected group.
		//	Defaults to the first element. But in case we use logic to change that later
		//	for some reason, we'll just get it appropriately here.
		const group = items.usergroups.groups[Groups.current];
		UpdateUsers(group ? group.Users : []);

		CheckEnabledButtons();

		_('#user-actions .edit').setAttribute('disabled', true);
		_('#user-actions .delete').setAttribute('disabled', true);
	});

})();