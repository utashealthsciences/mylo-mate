(function() {
	if(window.NewUpdatePosted) return;
	window.NewUpdatePosted = true;

	const dialog = document.createElement('div');
	document.body.appendChild(dialog);

	dialog.style.position = 'fixed';
	dialog.style.top = '10px';
	dialog.style.right = '17px';
	dialog.style.width = dialog.style.height = '300px';
	dialog.style.zIndex = 100000;
	dialog.style.overflow = 'hidden';
	dialog.style.borderRadius = '4px';
	dialog.style.boxShadow = '0 0 21px -3px grey';
	dialog.style.border = '1px #ccc solid';
	dialog.style.transition = 'opacity 0.5s';

	const iframe = document.createElement('iframe');
	iframe.src = chrome.extension.getURL('app/notification.html');
	iframe.style.width = iframe.style.height = '300px';
	iframe.style.border = 'none';
	dialog.appendChild(iframe);
	
	window.addEventListener('message', e => {
		const msg = e.data;
		if (e.origin + '/' === chrome.extension.getURL('')) {
			switch(msg) {
				case 'close': {
					chrome.runtime.sendMessage({ request: 'closeNotification' });
					break;
				}
			}
		}
	});

	const close = () => {
		dialog.addEventListener('transitionend', () => {
			document.body.removeChild(dialog);
			window.NewUpdatePosted = false;
		});
		dialog.style.opacity = 0;
	};

	chrome.runtime.onMessage.addListener(function (r) {
		if(r.command == 'close') close();
	});
	
})();