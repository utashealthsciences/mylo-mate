/*global tinymce*/
(function() {
	if(window.RubricEditor) return;
	window.RubricEditor = true;

	if(!store.mylo.floatingRubricEditors) return;

	// Shorthand elements for getting elements.
	//	Usage: 
	//		_('selector')...					Get one
	//		_('selector',true)...				Get all
	//		_('selector')._('sub-selector')...	Get subchild from result.
	var _ = (r, all) => all ? document.querySelectorAll(r) : document.querySelector(r);
	Element.prototype._ = function (r, all) { return all ? document.querySelectorAll(r) : document.querySelector(r); };

	// Grab our toolbar, and make sure we don't remove it later by adding a custom class.
	const tb = _('.d2l-htmleditor .d2l-htmleditor-toolbar');
	tb.classList.add('d2l-toolbar-autopos');

	// Hide the rest of our heading toolbars (keep the spellcheck, etc).
	_('.d2l-htmleditor .d2l-htmleditor-toolbar:not(.d2l-htmleditor-footer):not(.d2l-toolbar-autopos)', true).forEach(r => r.style.display = 'none');
	
	const checkIfHidden = () => {
		if (_('.d2l-htmleditor-hide1-hidden', true).length > 0) {
			const html = _('.d2l-htmleditor-component-container');

			const clearBtn = document.createElement('span');
			clearBtn.className = 'd2l-htmleditor-toolbar-item';
			clearBtn.innerHTML = '<span class="d2l-htmleditor-button" id="clear-formatting" role="button" tabindex="0" title="Clear Formatting"></span>';
			[...html.querySelectorAll('.d2l-htmleditor-group')].pop().appendChild(clearBtn);

			const all = document.createElement('div');
			all.classList.add('editor-box');
			all.innerHTML = `<span class="legend">Apply to all</span>
	<div class="d2l-htmleditor-group d2l-htmleditor-hide1">
		<span class="d2l-htmleditor-toolbar-item">
			<a class="d2l-htmleditor-button" id="all_align_left" role="button" tabindex="0" title="Align Left">
				<img class="d2l-htmleditor-buttonmenuitem-icon" style="background-image: url(https://s.brightspace.com/lib/ilp/d2l/img/0/sprites/D2L.LP.Web.UI.Desktop.Controls.HtmlEditor.16E4A96862AADC16C03A4B213BA6E674.png); background-position: 0 -496px; background-repeat: no-repeat; width: 16px; height: 16px;" src="/d2l/img/lp/pixel.gif" alt="">
			</a>
		</span>
		<span class="d2l-htmleditor-toolbar-item">
			<span class="d2l-htmleditor-button" id="all-clear-formatting" role="button" tabindex="0" title="Clear Formatting"></span>
		</span>
	</div>
	<div class="d2l-clear"></div>`;

			const selected = document.createElement('div');
			selected.classList.add('editor-box');

			const label = document.createElement('span');
			label.classList.add('legend');
			label.innerText = 'Apply to selection';
			selected.appendChild(label);

			const container = html.cloneNode();
			container.appendChild(selected);
			container.appendChild(all);
			
			html.parentNode.insertBefore(container, html);
			
			selected.appendChild(html);

			_('#all_align_left').addEventListener('click', function (e) {
				tinymce.editors.forEach(editor => {
					editor.getBody().querySelectorAll('p').forEach(e => {
						e.style.textAlign = 'left';
						e.setAttribute('data-mce-style', e.getAttribute('style'));
					});
				});
			});

			const remove_tags = (content, allowed=[]) => {
				let d = document.createElement('div');
				d.innerHTML = content.getContent().replace(/<p[^>]*?>&nbsp;<\/p>/gi, '');
				let tags = [...d.querySelectorAll(`*`)].filter(node => !allowed.includes(node.nodeName.toLowerCase()));
				tags.forEach(tag => tag.replaceWith(...tag.childNodes));
				return d.innerHTML;		
			};

			clearBtn.addEventListener('click', e => {
				tinymce.activeEditor.setContent(remove_tags(tinymce.activeEditor, ['p', 'br']));
			});

			_('#all-clear-formatting').addEventListener('click', e => {
				tinymce.editors.forEach(editor => editor.setContent(remove_tags(editor, ['p', 'br'])));
			});

			_('.d2l-htmleditor-hide1-hidden', true).forEach(btn => btn.classList.remove('d2l-htmleditor-hide1-hidden'));
			_('.d2l-htmleditor-component-toggle').classList.add('d2l-hidden');
			
			const buttonReappearFix = new MutationObserver(muts => {
				if (!_('.d2l-htmleditor-component-toggle').classList.contains('d2l-hidden')) {
					_('.d2l-htmleditor-hide1-hidden', true).forEach(btn => btn.classList.remove('d2l-htmleditor-hide1-hidden'));
					_('.d2l-htmleditor-component-toggle').classList.add('d2l-hidden');
				}
			});

			buttonReappearFix.observe(_('.d2l-htmleditor-component-toggle'), {attributes: true, attributeFilter: ['class']});

		}
		else setTimeout(checkIfHidden, 10);
	};
	checkIfHidden();	

})();