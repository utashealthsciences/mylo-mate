// Holds the default configuration of the app.
const defaults = {
	"manager": {
		"adjustTable": false,
		"enablePending": false,
		"recordsPerPage": 50,
		"addQuicklinks": false,
		"addBUM": false,
		"displayRowColours": true
	},
	"mylo": {
		"adjustButtons": false,
		"enhancements": true,
		"overrideGrades": false,
		"enableUnitOutline": false,
		"highlightRowBackgrounds": true,
		"floatingRubricEditors": true
	},
	"editor": {
		"defaultShow": true,
		"showDevTools": false,
		"keepSmall": true,
		"enablePreviewWindow": true,
		"allLinksInNewTab": false
	},
	"other": {
		"showDev2": false,
		"makeAccessible": false
	},
	"usergroups": {
		"enabled": false,
		"groups": {
			"_raw": true
		}
	}
};

const devDefaults = {
	"manager": {
		"adjustTable": true,
		"enablePending": true,
		"recordsPerPage": 500,
		"addQuicklinks": true,
		"addBUM": true,
		"displayRowColours": true,
	},
	"mylo": {
		"adjustButtons": true,
		"enhancements": true,
		"overrideGrades": true,
		"enableUnitOutline": false,
		"highlightRowBackgrounds": true,
		"floatingRubricEditors": true,
	},
	"editor": {
		"defaultShow": true,
		"enablePreviewWindow": true,
		"allLinksInNewTab": false
	},
	"other": {
		"showDev2": true
	}
};

// Gets all settings for the extension
chrome.storage.sync.get(null, function(items) {
	// Fill the existing data with any blanks from our default object.
	const data = $.extend(true, {}, defaults, items);
	
	// Set the inline variables that need to be called from within injected scripts.
	// These are currently a stringified data object (all our settings), and the extension ID for message handling.
	const inlineVariables = `var store = JSON.parse('${JSON.stringify(data)}'); var extensionID = "${chrome.runtime.id}"`;
	
	// If we're not looking at a page with the chrome-extension protocol, then inject our inline variables.
	if(!window.location.href.startsWith('chrome-extension://')) {
		document.head.appendChild(document.createElement('script')).innerHTML = inlineVariables;
	}

	// Make sure that we update our dataset to fill any blanks from earlier.
	chrome.storage.sync.set(data);
}); 