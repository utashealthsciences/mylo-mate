function AddStaffMember(rowID, ShowMessages) {
	var Duplicates = false;
	var rowData = $("#grdDialogResults").jqGrid().getRowData(rowID);
	$("#grdStaff tr").each(function (index, value) { if ($(value).find("td:first input").val() == rowID) Duplicates = true; });
	if (!Duplicates) {
		// The URL that this request is posted to is the same UUID as the current page!
		$.post('/UnitRequest/AddStaff/ae300953-b92d-4905-b2db-4db744f18e8e', { 
			StaffID: rowData.StaffID,
			GivenName: rowData.GivenName,
			PreferredName:rowData.PreferredName,
			Surname: rowData.Surname,
			Username: rowData.Username,
			Department: rowData.Schools

			// Is there a way to add the default role perhaps?
		},
		function (data) {
			$("#grdStaff tbody").append(data);
		});
	}

	if (ShowMessages && Duplicates) {
		alert('The staff member "' + rowData.GivenName + ' ' + rowData.Surname + '" has already been added to this unit');
	}
}