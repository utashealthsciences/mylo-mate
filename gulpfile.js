const gulp = require('gulp');
const babel = require('gulp-babel');
const browserify = require('browserify');
const babelify = require('babelify');
const source = require('vinyl-source-stream');

gulp.task('downgrade', () => {
	return gulp.src('src/js/es6/*.js')
		.pipe(babel({ presets: ['es2015'] }))
		.pipe(gulp.dest('src/js/mylo-editor'));
});

gulp.task('watch', function() {
	gulp.watch('src/js/es6/*.js', ['downgrade']);
	//gulp.watch('src/react/**/*.js', ['bundle']);
});

gulp.task('bundle', function() {
	return browserify({
			extensions: ['.js'],
			entries: ['src/react/app.js'],
		})
		.transform(babelify.configure({
			presets: ['es2015', 'react'],
			ignore: /(bower_components)|(node_modules)/
		}))
		.bundle()
		.on('error', function(err) { console.log('Error : ' + err.message); })
		.pipe(source('bundle.js'))
		.pipe(gulp.dest('src/js/mylo-editor'));
});

gulp.task('default', ['downgrade', 'watch']);
