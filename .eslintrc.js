module.exports = {
    "env": {
        "browser": true,
		"es6": true,
		"jquery": true
	},
	"parserOptions": {
		"ecmaVersion": 6,
		"ecmaFeatures": {
			"experimentalObjectRestSpread": true
		}
	},
    "extends": "eslint:recommended",
    "rules": {
        "indent": [
			"warn",
			"tab",
			{
				"SwitchCase": 1
			}
        ],
        "quotes": [
            "warn",
            "single", 
			{ "allowTemplateLiterals": true }
        ],
        "semi": [
            "error",
            "always"
        ],
		"no-unused-vars": 1,
    	"no-console": 0
    },
	"globals": {
		"chrome": false,
		"tinyMCE": false
	}
};