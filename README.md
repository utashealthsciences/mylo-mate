# MyLO MATE Project
##### Project managed by Kevin Lyall and Connor Deckers
##### Current version: v2.0.10

### Details
MyLO MATE is now an extension for Google Chrome, [available here](https://chrome.google.com/webstore/detail/mylo-quicklinks/bbmpfljhlpdmialnpoakplledongdcom). It is an extension that enhances MyLO, MyLO Manager and Echo360.

### Daylight Icon Sets
Daylight icon sets can be found here: https://github.com/BrightspaceUI/icons

### Firefox and Chrome API differences
Incompatabilities and inconsistancies between Mozilla Firefox and Google Chrome can be [found here](https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Chrome_incompatibilities).

### MyLO Manager

* Allows creation of custom user groups to allow easy adding of staff to units
* Adds a Bulk User Management (BUM) CSV export button at bottom of list
* Rows coloured according to start and end date of unit
* Default rows per page is saved
* Expands the table width to 100%, allowing the user to widen columns and use the full width of the browser.
* Re-enables the unit link (and Quicklinks) where the status is Pending.
* Adds a series of links below each unit link, hover for details. Each link goes directly to a part of MyLO without having to go via the home page. 
* Adds a legend at the bottom:
	* **Co** = Content
	* **Cl** = Classlist   
	* **F**= File Manager   
	* **Di** = Discussions   
	* **As** = Assignments   
	* **Gp** = Groups   
	* **Gd** = Grades   
	* **Gs** = Grades Settings   
	* **Q**= Quizzes   
	* **Ru** = Rubrics   
	* **CS** = Classlist Settings
	* **IE** = Import/Export Components

### MyLO

* Offers option to show link to unit outline in nav bar
* Updates external links to open in new tab where appropriate
* Adds shortcuts direct to Restrictions, Assessment and Submission Views to Quiz Management and Restrictions and Turnitin to Assignment Folders lists
* Overrides nav bar Grades link to go to Manage Grades instead of student grades
* Adds direct links to Manage Files and Unit Admin tools (leaving drop-down links in place)
* HTML Editor includes an ICB icon that opens a panel allowing easy addition of ICB template content, negating the need to copy-paste.

### Echo360

* Download with one click all videos in an Echo360 'course'

Please note that the developers of this extension have no control over changes to Echo360, MyLO Manager or MyLO, and as a result some aspects may stop working without notice. We'll endeavour to keep it updated. Please let us know if you have any suggestions for features, improvements or any bug reports. 

If you have any suggestions for new features, or would like to report a bug, please email us at: `MyLO.MATE@utas.edu.au`

For release notes and further information, visit the [MyLO MATE UTAS page](https://secure.utas.edu.au/faculty-of-health-intranet/mylo-resources/mylo-mate).

## Developer Notes

### Packaging
Little preparation is required for the packaging of the application. Archive the contents of the `src` directory into a `.zip` file, ensuring that `manifest.json` is updated to reflect the latest release version.

### Uploading
Go to the Chrome Web Store, and under `Settings` select `Developer Dashboard`. There, you will have the capacity to visit the plugin dashboard, and upload an update to the plugin.
