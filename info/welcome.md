[![MyLO MATE][logo]][download]
MyLO MATE is an extension for Google Chrome, [available here][download]. It is an extension that enhances MyLO, MyLO Manager and Echo360. For detailed information on how to use the different features available, please consult the [User Guide][user guide].

# Contents
- [Overview](#overview)
- [What's New] (#whatsnew)
- [Release Notes] (#release-notes) 

# Overview {overview}
## MyLO Manager {info-mylo-manager}
- Allows creation of custom user groups to allow easy adding of staff to units
- Adds a Bulk User Management (BUM) CSV export button at bottom of list
- Rows coloured according to start and end date of unit
- Default rows per page is saved
- Expands the table width to 100%, allowing the user to widen columns and use the full width of the browser
- Re-enables the unit link (and unit quick links) where the status is Pending
- Adds a series of links below each unit link, hover for details. Each link goes directly to a part of MyLO without having to go via the home page. Adds a legend at the bottom:  
  - **Co** = Content   
  - **Cl** = Classlist   
  - **F** = File Manager   
  - **Di** = Discussions   
  - **As** = Assignments   
  - **Gp** = Groups   
  - **Gd** = Grades   
  - **Gs** = Grades Settings   
  - **Q** = Quizzes   
  - **Ru** = Rubrics   
  - **CS** = Classlist Settings  
  - **IE** = Import/Export Components

## MyLO {info-mylo}
- Offers option to show link to unit outline in nav bar (only works with single unit outline PDFs)
- Quizzes, Assignment Folders and Grades all get shortcuts to popular tabs within each tool (e.g. Restrictions, Assessment, Turnitin)
- Option to add direct links to Unit Outline, Manage Files and Unit Admin tools. Note the unit outline link will only work for units with a single PDF unit outline
- HTML Editor includes an ICB icon that opens a panel allowing easy addition of ICB template content, negating the need to copy-paste
- Links that would be broken in the HTML Editor are automatically fixed. An option is available to make all external links to open in a new tab 
- Rubric editing pages are changed to consolidate the editing buttons, and introduce *Apply to all* buttons for left alignment and clear formatting
- Unit in search are coloured according to approximate start and end date of unit

## Echo360 {info-echo360}
- Download with one click all videos in an Echo360 'course'

Please note that the developers of this extension have no control over changes to Echo360, MyLO Manager or MyLO, and as a result some aspects may stop working without notice. We'll endeavour to keep it updated. Please let us know if you have any suggestions for features, improvements or bugs.

If you have any suggestions for new features, or would like to report a bug, please Email us at: <MyLO.MATE@utas.edu.au>

# What's New - Version 2.1.7 {whatsnew}
*21 December 2017*

## Echo360
On 20 December a new version of Echo360 was released that changed the way video downloads are handled, and this disabled the Echo360 download functionality. This version changes the download method, reinstating the bulk downloading functionality. 

# Feature Highlight

## Options!
MyLO MATE has many options switched off by default that you might find useful. Click on the MyLO MATE icon and select `Options`. The tabs `MyLO Manager`, `MyLO`, `MyLO Editor`, `User Groups` (if enabled) and `Other` all contain various categorised options that you can tick to enable. Hover over the `?` icon next to an option for more information. If you have any suggestions or questions, please send us an Email: <MyLO.MATE@utas.edu.au>

![Options](/etc/whatsnew/option.png)

# Release Notes {release-notes}
/--
/^^ Version 2.1.7 (current version)
*21 December 2017*
### Echo360
* Implement a new method for bulk downloading videos

/^^ Version 2.1.6
*7 December 2017*
### MyLO
* Shortcuts to Restrictions [r] and Assessment [a] added to the Discussions screen
* Fixed bug where navigation bar enhancements (unit outline, file manager, unit admin) weren't appearing on certain pages in MyLO
* Fixed bug where some developer features appeared when in a unit as a student (e.g. Manage Grades override)

### MyLO Manager
* Sometimes `Undefined` would appear in the User Groups tool (e.g. when creating a new unit)

### Other
* Update notification panel disappears when link is clicked, and can easily be disabled for certain releases (like this one)

/^^ Version 2.1.5
*1 December 2017*

### MyLO 
- The dates used for highlighting current units changed to extend beyond default semester dates to ensure they remain green (or blue) after exam period finishes (while staff are still working with the unit)
- Grades link no longer goes to a forbidden page if current role is student
- Unit outline link corrected where unit code changed after being initially ordered

### Echo360
- Fix bug where downloads would fail where unit title contained certain punctuation (e.g. backslash)

### Other
- Context added to references to the Bulk User Management (BUM) Tool  
- Options storage method adjusted to be more robust

/^^ Version 2.1.4 
*17 November 2017*

### MyLO
- Allow ICB panel to operate when editing in full screen mode
- When bulk deleting discussion forums and topics, the Delete button remains disabled when Select All is ticked. MyLO MATE fixes this D2L bug
- Introduces three new buttons in File Manager: *Select all folders*, *Select all linked files* and *Select all unlinked files*
- Future units no longer highlighted red. Instead, they are left with a white background
- *Strip Tags* option introduced with ICB panel (quickly removes unneeded tags in the HTML). Enabled as part of Options > Enable Developer Tools

### Other
- In previous versions when a new version of MyLO MATE was released, a manatory new tab was shown. Now we have a much less intrusive panel notification instead

/^^ Version 2.1.3 
*27 October 2017*

### MyLO
- Adjust contrast of current units that are highlighted in accessibility mode to make it easier to read
- Streamline rubric editing functionality and add single *clear format* button

### Other
- Change wording of error message that is shown when user group adjustment is attempted when not logged into MyLO Manager, so the user knows what's wrong
- Change wording and function of ICB secondary headings option (tick to enable, instead of tick to disable)

/^^ Version 2.1.2 
*13 October 2017*

### MyLO
- Background colours for units are changed according to approximate semester start and end dates
- Consolidated rubric editing toolbar introduced
- Subtitle now appears under unit title in Daylight indicating semester and unit type
 
### Other 
- Various bug fixes and visual improvements

/^^ Version 2.1.1 
*6 October 2017*

### Other
- Remove weird *Message* link in MyLO Mate panel
- Various bug fixes and visual improvements

/^^ Version 2.1.0  
*29 September 2017*

### MyLO Manager
- Groups management functionality in the Staff tab now improved to be much more intuitive

### Other
- Help (?) icons now show help on click as well as on hover
- Extension name changed to MyLO MATE 
- Notifications of new features introduced
- Various bug fixes and visual improvements

/^^ Version 2.0.10
*15 September 2017*

### MyLO
- Fixes direct Reading List links: After editing an HTML page, any corrupted ezproxy links will automatically be fixed when you Update (save) the file

### MyLO Manager
- In the Staff tab, you can now add a comma separated list of staff to a unit using the Add Username List to Unit... button
- Improved user groups usability

### Other
- Quiz, Assignment Folders, Grades link enhancement option now controlled by the one checkbox in the options
- Various bug fixes and visual improvements

/^^ Version 2.0.9
*8 September 2017*

### Other
- Introduced 'Accessibility mode' which changes MyLO Manager unit status colouring to be easier for those with colour blindness
- Various bug fixes and visual improvements

/^^ Version 2.0.8
*6 September 2017*

### MyLO Manager
- Custom User Groups can now be created to allow adding multiple users of varying roles to a unit with one click
- Department drop-down list in Find Staff window reduced to a manageable width

### MyLO
- Option to add link to unit outline in navigation bar introduced
- Invalid links to external HTTP sites are now automatically fixed upon saving an HTML file (changed to open in new tab)
- Options introduced to allow all external links to open in a new tab either automatically or on demand

### Other
- Various bug fixes and visual improvements

/^^ Version 2.0.7
*11 August 2017*

### MyLO
- Add Submission Views link alongside Assessment and Restrictions links next to each quiz title
- Add link to ICB home page in ICB panel
- Now Daylight compatible

### Other
- Introduce Developer Defaults button, default on install is for most options to be off
- Add option to include link to dev2 (Daylight) in extension panel
- Various bug fixes and visual improvements

/^^ Version 2.0.6
*28 July 2017*

### Other
- Add Options shortcut link to icon popup panel
- Various bug fixes and visual improvements

/^^ Version 2.0.5
*21 July 2017*

### MyLO
- ICB panel introduced, visual 'preview' representation of each ICB element in panel on hover

### Other
- Reworked Options panel to tabbed system
- Various bug fixes

/^^ Version 2.0.4
*14 July 2017*

### MyLO
- Assignment Folders links now has direct links to Restrictions and Turnitin

### Other
- Extension icon now opens panel of relevant links
- Add Echo360 download all videos link
- Various bug fixes, including a nasty one that caused all options to be turned off

/^^ Version 2.0.3
*7 July 2017*

### MyLO
- Add direct links to Restrictions and Assessment in the Quizzes page
- Initial release of ICB direct interface

/^^ Version 2.0.2
*30 June 2017*

### Other
- Options revamped to allow individual features to be turned off

### MyLO Manager
- Bulk Unit Management (BUM) tool button added at bottom of unit list
- Number of rows per page now able to be overridden either in Options or in MM

/^^ Version 2.0.1
*23 June 2017*

### Other
- Extension options introduced
- Clicking extension icon opens MyLO Manager in a new tab
- Other miscellaneous bug-fixes and improvements

### MyLO Manager
- Links to MyLO units re-inserted even if they are Pending
- Import/Export Components Quicklink added
- Number of rows per page able to be set in Options

### MyLO
- Icon added to link directly to Unit Admin
- Icon added to link directly to Manage Files

/^^ Version 2.0.0
*20 June 2017*

### Initial Release
- Quicklinks control panel added to MyLO Manager
- Force MyLO Manager table to extend the full width of the browser

--/

The older obsolete MyLO Quicklinks page is [available here](./?a=988460) for reference.

[logo]: https://secure.utas.edu.au/faculty-of-health-intranet/mylo-resources/mylo-mate/icon128.png "MyLO MATE"
[download]: https://secure.utas.edu.au/faculty-of-health-intranet/mylo-resources/mylo-mate/download
[user guide]: https://secure.utas.edu.au/faculty-of-health-intranet/mylo-resources/mylo-mate/user-guide