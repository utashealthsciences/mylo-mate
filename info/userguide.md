MyLO MATE is an extension for Google Chrome that enhances MyLO, MyLO Manager and Echo360. This user guide is a work in progress. Would you like to contribute? Please [let us know](mailto:MyLO.MATE@utas.edu.au)!

# Default Options
When first installed, the extension defaults to ‘Lecturer mode’ and many of the options are off. If you are a developer or manage many units, you may wish to change to ‘Developer mode’. Right-click the MyLO MATE icon in the top-right corner of Chrome, and select Options:

![MyLO MATE Options](/etc/images/main-options1.png)
 
Select `Defaults…` to reset to the options that will suit most lecturers, or select `Developer Defaults…` to turn on most of the other options, suitable for developers. 

![MyLO MATE Options](/etc/images/main-options2.png)
 
# MyLO MATE Panel
Left-click the MyLO MATE icon in the top-right corner of Chrome to show the MyLO MATE panel:

![MyLO MATE Panel](/etc/images/main-panel.png)
 
The panel offers a quick, convenient way to open access following links:

* MyLO <https://mylo.utas.edu.au/>
* MyLO Manager <https://mylo-manager.utas.edu.au/>
* Echo360 <https://echo360.org.au/>
* DEV2 (only if the option is turned on) <https://d2ldev2.utas.edu.au/>
* Options (for MyLO MATE)
* How to Use (a link to this document)
* What’s new in v2 <https://secure.utas.edu.au/faculty-of-health-intranet/mylo-resources/mylo-mate#whatsnew>
 
# Options

/--

/^^ MyLO Manager

![MyLO Manager Options](/etc/images/options-mylo-manager.png)

## Add Quicklinks

## Adjust table width to 100%

## Force links to units with pending updates to open

## Units per page

## Add Bulk User Management (BUM) CSV Export button at bottom of table

## Highlight row backgrounds

## Enable user groups

/^^ MyLO 

![MyLO Options](/etc/images/options-mylo.png)

## Adjust MyLO Toolbar buttons

## Quizzes, Assignment Folders, Grades link enhancements

## Override Grades link

## Enable Unit Outline shortcut

/^^ MyLO Editor

![MyLO Editor Options](/etc/images/options-mylo-editor.png)

## Show ICB Sidebar on open

## Enable Developer Tools

## Disable Secondary Headings

## Enable Preview

## Make All Links Open in New Tab

/^^ User Groups

![User Groups Options](/etc/images/options-user-groups.png)

Please note this options tab is only available if the `Enable user groups` option is enabled in the `MyLO Manager` tab.

## Creating a new group

Select the `Add` group button:

![Add New Group](/etc/images/options-user-groups-new-group1.png)

Type a name for your new group and select `OK`:

![Add New Group](/etc/images/options-user-groups-new-group2.png)

## Add users to a group

Select a group from the drop-down list, then select the `Add` users button:

![Add Users](/etc/images/options-user-groups-add-users1.png)

Type in the usernames of the users you want to add to the selected group (separated by commas), select the role (Support, Lecturer, etc.) and then select OK:

![Add Users](/etc/images/options-user-groups-add-users2.png)

/^^ Other

![Other Options](/etc/images/options-other.png)

## Show DEV2 link

## Accessibility mode

--/