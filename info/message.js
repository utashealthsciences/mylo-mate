showdown.extension('accordian', () => {
	var accordians = [];
	return [
		{
			type: 'lang',
			regex: /\/--([^]+?)--\//gi,
			replace: (s, match) => {
				let items = [];
				let regex = /\/\^\^(?:(?!--\/|\/\^\^)[\s\S])*/gi;
				let matches = match.match(regex)
					.map(item => item.replace('/^^', '').trim())
					.forEach(item => {
						const lines = item.split('\n');
						const heading = lines.shift().trim();
						const body = lines.join('\n');
						items.push({heading, body});
					});
				accordians.push(items);
				var n = accordians.length - 1;
				return `%ACCORDIAN${n}%`;
			}
		},
		{
			type:'output',
			filter: (text, c, options) => {
				const conv = new showdown.Converter({ ...options, extensions: []});
				for (let i = 0; i < accordians.length; i++) {
					let html = '<div id="accordion-asset-listing" class="accordion" role="tabpanel">';
					accordians[i].forEach(row => {
						const ds = (new Date()).getTime() + Math.random().toString().replace('.', '');
						html += `
						<section class="accordion-group" role="tab" aria-selected="false">
							<header class="accordion-heading">
								<h3><a aria-controls="${row.heading.toLowerCase().replace(/[^\w\s\d]/gi, '').replace(/[\s]/g, '-')}" href="#${ds}" class="accordion-toggle collapsed" data-toggle="collapse"
									data-parent="#accordion-asset-listing">${row.heading}</a></h3>
							</header>
						
							<div id="${ds}" class="accordion-body collapse" aria-hidden="true" role="tabpanel" style="height: 0px;">
								<div class="accordion-inner">
									${conv.makeHtml(row.body.trim())}
								</div>
							</div>
						</section>
						`;
					});				
					html += '</div>';
					let pattern = `<p>%ACCORDIAN${i}% *<\/p>`;
					text = text.replace(new RegExp(pattern, 'gi'), html);
				}
				accordians = [];
				return text;
			}
		}
	];
});

const converter = new showdown.Converter({
	extensions: ['accordian'],
	customizedHeaderId: true,
	rawHeaderId: true
});
const url = new URL(window.location);
let path = url.href.split('/'); let u = path.pop().split('.')[0]; path = path.join('/') + '/';
$.ajax({
	url: path+u+'.md',
	success: (md) => { 
		const html = converter.makeHtml(md); 
		document.querySelector('#md').innerHTML = html;
		document.head.querySelector('title').innerText = document.querySelector('#md h1').innerText;
	}
});